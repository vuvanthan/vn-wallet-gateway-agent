package vn.truemoney.ewallet.gateway.security.jwt;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import vn.truemoney.ewallet.gateway.businesslogic.transaction.ITransactionLogLogic;
import vn.truemoney.ewallet.gateway.common.Debugger;
import vn.truemoney.ewallet.gateway.common.util.Utils;
import vn.truemoney.ewallet.gateway.persistence.model.transaction.TransactionLog;

@Aspect
@Component
public class LoggingAspect {
	@SuppressWarnings("rawtypes")
	private final static Class DEBUGGED_CLASS = LoggingAspect.class;
	private Debugger debugger = new Debugger(DEBUGGED_CLASS);

	@Autowired
	protected ITransactionLogLogic transactionLogLogic;

	@Around("execution(* vn.truemoney.ewallet.gateway.*.rest.*.*(..))")
	public Object logAfterReturning(ProceedingJoinPoint point) throws Throwable {
		// final long startTime = System.currentTimeMillis();
		ResponseEntity<?> response = null;
		
		TransactionLog transactionLogRequest = new TransactionLog();
		TransactionLog transactionLogResponse = null;
		int type;
		try {
			// SystemLog systemLog = new SystemLog();
			try {
				Object[] obj = point.getArgs();
				HttpServletRequest request = (HttpServletRequest) obj[obj.length - 1];
				String ip = Utils.getIP(request);
				
				String requestBody = IOUtils.toString(request.getInputStream());
				String sessionId = request.getHeader("Session-Id");
				String requestId = request.getHeader("Request-Id");
				Date requestDate = new Date();
				String method = request.getMethod();
				type = 0;				
				final String requestUrl = request.getRequestURI() != null ? request.getRequestURI().toString() : null;
				
				if(sessionId == null) {
					sessionId = "0";
				}
				transactionLogRequest.setSessionId(sessionId);
				transactionLogRequest.setRequestId(requestId);
				transactionLogRequest.setIp(ip);
				transactionLogRequest.setRequestURI(requestUrl);
				
				transactionLogRequest.setBody(requestBody);
				transactionLogRequest.setRequestTime(requestDate);
				transactionLogRequest.setResponseTime(new Date());
				transactionLogRequest.setTypegw(type);
				transactionLogRequest.setMethod(method);

				transactionLogLogic.saveTransactionLog(transactionLogRequest, 1L);

				

			} catch (Exception e) {
				debugger.errorException(e.getMessage(), e);
			}
			
			response = (ResponseEntity<?>) point.proceed();
			try {
				if (transactionLogRequest.getId() != 0) {
					type = 1;
					
					String respBody = Utils.objectToJson(response.getBody());
					
					transactionLogResponse = transactionLogRequest.clone();					
					transactionLogResponse.setTypegw(type);
					transactionLogResponse.setBody(respBody);
					transactionLogResponse.setParent(transactionLogRequest.getId());
					transactionLogRequest.setResponseTime(new Date());
					
					transactionLogLogic.saveTransactionLog(transactionLogResponse, 1L);
				}
			} catch (Exception e) {
				debugger.errorException(e.getMessage(), e);
			}

		} catch (Exception e) {
			debugger.errorException(e.getMessage(), e);
		} 
		return response;
	}

}
