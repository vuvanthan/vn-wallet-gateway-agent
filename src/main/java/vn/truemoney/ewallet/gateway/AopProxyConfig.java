package vn.truemoney.ewallet.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import vn.truemoney.framework.service.monitor.PerformanceMonitoringAdvice;
import vn.truemoney.framework.service.response.ResponseCodeAdvice;

@Configuration
public class AopProxyConfig {
  @Autowired
  private ResponseCodeAdvice responseCodeAdvice;
  @Autowired
  private PerformanceMonitoringAdvice performanceMonitoringAdvice;

}
