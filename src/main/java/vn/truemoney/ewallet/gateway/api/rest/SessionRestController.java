package vn.truemoney.ewallet.gateway.api.rest;

import java.io.IOException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import vn.truemoney.ewallet.gateway.common.Debugger;
import vn.truemoney.ewallet.gateway.common.util.ResponseUtils;
import vn.truemoney.ewallet.gateway.common.util.Utils;
import vn.truemoney.ewallet.gateway.contract.session.SessionRequest;
import vn.truemoney.ewallet.gateway.contract.session.SessionResponse;
import vn.truemoney.ewallet.gateway.persistence.model.session.Session;

@RestController
@RequestMapping(value = "/api/mobile/v1/get_session")
public class SessionRestController extends AbstractRestController {
	@SuppressWarnings("rawtypes")
	private Class DEBUGGED_CLASS = SessionRestController.class;
	private final Debugger debugger = new Debugger(DEBUGGED_CLASS);
	
	@Value("${application.session.time-out}")
	private Long timeOut;
	
	@Value("${application.password-default}")
	private String passwordDefault;

	@RequestMapping(value = "", method = { RequestMethod.POST }, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> actionGetSession(@RequestBody String requestBody, HttpServletRequest req)
			throws IOException, InterruptedException {
		ResponseUtils.getRequestLog(req, requestBody, DEBUGGED_CLASS);
		final String ip = Utils.getIP(req);
		SessionResponse sessionResponse = null;
		String signature = req.getHeader("Signature");
		String requestId = req.getHeader("Request-Id");
		String deviceId = req.getHeader(DEVICE_ID);
		try {
			
			ResponseUtils.verifySignature(signature,passwordDefault,requestBody, true);
			
			SessionRequest sessionRequest = gson.fromJson(requestBody, SessionRequest.class);
			
			// create session for mobile
			Session session = new Session();
			session.setSessionId(Utils.generateSessionId());
			session.setSessionKey(Utils.generateRandomString(32));
			session.setRequestId(requestId);
			session.setIp(ip);
			session.setDeviceId(deviceId);
			session.setAppVersion(sessionRequest.getApp_version());
			session.setActivedTime(new Date());
			session.setExpiredTime(new Date((new Date().getTime() + timeOut)));
			session.setStatus(Session.Status.ACTIVED);
			
			sessionLogic.createSession(session, 1L);
			debugger.logDebug("CREATE SESSION", session);
			
			// create session response
			sessionResponse = new SessionResponse();
			sessionResponse.setSessionId(session.getSessionId());
			sessionResponse.setSessionKey(session.getSessionKey());

			return new ResponseEntity<SessionResponse>(sessionResponse, Utils.getHttpHeaders(), HttpStatus.OK);
		} catch (Exception e) {
			debugger.errorException(e.getMessage(), e);
			return ResponseUtils.returnBadGateway();
		}
	}
}
