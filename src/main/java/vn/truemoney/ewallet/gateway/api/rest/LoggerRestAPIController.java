package vn.truemoney.ewallet.gateway.api.rest;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vn.truemoney.ewallet.gateway.common.Debugger;
import vn.truemoney.ewallet.gateway.contract.logger.BasicResponse;
import vn.truemoney.ewallet.gateway.contract.logger.LoggerLevelRequest;

@RestController
@RequestMapping("/system")
public class LoggerRestAPIController extends AbstractRestController {
	@SuppressWarnings("rawtypes")
	private final static Class DEBUGGED_CLASS = LoggerRestAPIController.class;
	private Debugger debugger = new Debugger(DEBUGGED_CLASS);

	@RequestMapping(value = "/loggers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> reloadLogLevel(@RequestBody LoggerLevelRequest request, HttpServletRequest httpRequest) {
		BasicResponse response = null;
		try {
			if (request == null || (request != null && StringUtils.isBlank(request.getConfiguredLevel()))) {
				return new ResponseEntity<BasicResponse>(HttpStatus.BAD_REQUEST);
			}

			String level = request.getConfiguredLevel();
			String packageName = "vn.truemoney.ewallet";
			if (StringUtils.isNotBlank(request.getPackageName())) {
				packageName = request.getPackageName();
			}
			Logger logger = LogManager.getLogger(packageName);

			Level logLevel = null;
			if (level.equalsIgnoreCase(Level.ALL.toString())) {
				logLevel = Level.ALL;
			} else if (level.equalsIgnoreCase(Level.DEBUG.toString())) {
				logLevel = Level.DEBUG;
			} else if (level.equalsIgnoreCase(Level.ERROR.toString())) {
				logLevel = Level.ERROR;
			} else if (level.equalsIgnoreCase(Level.FATAL.toString())) {
				logLevel = Level.FATAL;
			} else if (level.equalsIgnoreCase(Level.INFO.toString())) {
				logLevel = Level.INFO;
			} else if (level.equalsIgnoreCase(Level.OFF.toString())) {
				logLevel = Level.OFF;
			} else if (level.equalsIgnoreCase(Level.TRACE.toString())) {
				logLevel = Level.TRACE;
			} else if (level.equalsIgnoreCase(Level.WARN.toString())) {
				logLevel = Level.WARN;
			} else {
				return new ResponseEntity<BasicResponse>(HttpStatus.BAD_REQUEST);
			}
			logger.setLevel(logLevel);
			response = new BasicResponse();
			response.setCode("00");
			response.setMsg("Success");
		} catch (Exception e) {
			debugger.errorException(e.getMessage(), e);
			return new ResponseEntity<BasicResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<BasicResponse>(response, HttpStatus.OK);
	}	
}