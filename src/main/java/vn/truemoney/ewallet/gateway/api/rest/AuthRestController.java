package vn.truemoney.ewallet.gateway.api.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vn.truemoney.ewallet.gateway.common.Debugger;
import vn.truemoney.ewallet.gateway.common.util.ResponseUtils;
import vn.truemoney.ewallet.gateway.common.util.Utils;
import vn.truemoney.ewallet.gateway.contract.auth.RefreshTokenResponse;
import vn.truemoney.ewallet.gateway.contract.auth.SignInResponse;
import vn.truemoney.ewallet.gateway.contract.auth.SignUpResponse;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;
import vn.truemoney.ewallet.gateway.persistence.model.session.Session;

@RestController
@RequestMapping(value = "/api/mobile/v1/auth")
public class AuthRestController extends AbstractRestController {
	@SuppressWarnings("rawtypes")
	private final static Class DEBUGGED_CLASS = AuthRestController.class;
	private final Debugger debugger = new Debugger(DEBUGGED_CLASS);

	@RequestMapping(value = "/check_info", method = { RequestMethod.POST }, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> actionCheckInfo(@RequestBody String requestBody, HttpServletRequest req)
			throws IOException, InterruptedException {
		ResponseUtils.getRequestLog(req, requestBody, DEBUGGED_CLASS);
		String signature = req.getHeader(SIGNATURE);
		String sessionId = req.getHeader(SESSION_ID);
		String deviceId = req.getHeader(DEVICE_ID);
		SignUpResponse checkResponse = null;
		ResponseDTO response = null;

		try {
			Session session = sessionLogic.findBySessionId(sessionId);
			ResponseUtils.verifySession(session);
			ResponseUtils.verifySignature(signature, session.getSessionKey(), requestBody, false);

			// send data to proxy
			response = ewalletAuth.checkInfo(requestBody,deviceId);
			ResponseUtils.verifyProxyResponse(response);
			checkResponse = new SignUpResponse(response);

			// Create response form
			debugger.logDebug("RESPONSE BODY FOR CLIENT", Utils.masking2(checkResponse));
			return new ResponseEntity<SignUpResponse>(checkResponse, Utils.getHttpHeaders(), HttpStatus.OK);
		} catch (Exception e) {
			debugger.errorException(e.getMessage(), e);
			return ResponseUtils.returnError(e.getMessage(), response);
		} 
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/resend_otp", method = { RequestMethod.POST }, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> actionResendOTP(@RequestBody String requestBody, HttpServletRequest req)
			throws IOException, InterruptedException {
		ResponseUtils.getRequestLog(req, requestBody, DEBUGGED_CLASS);
		String signature = req.getHeader(SIGNATURE);
		String sessionId = req.getHeader("Session-Id");
		ResponseDTO response = null;

		try {
			Session session = sessionLogic.findBySessionId(sessionId);
			ResponseUtils.verifySession(session);
			ResponseUtils.verifySignature(signature, session.getSessionKey(), requestBody, false);

			// send data to proxy
			response = ewalletAuth.resendOTP(requestBody);
			ResponseUtils.verifyProxyResponse(response);

			// Create response form
			return new ResponseEntity(Utils.getHttpHeaders(), HttpStatus.OK);
		} catch (Exception e) {
			debugger.errorException(e.getMessage(), e);
			return ResponseUtils.returnError(e.getMessage(), response);
		}
	}

	@RequestMapping(value = "/verify_otp", method = { RequestMethod.POST }, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> actionVerifyOTP(@RequestBody String requestBody, HttpServletRequest req)
			throws IOException, InterruptedException {
		ResponseUtils.getRequestLog(req, requestBody, DEBUGGED_CLASS);
		String signature = req.getHeader("Signature");
		String sessionId = req.getHeader("Session-Id");
		SignUpResponse verifyResponse = null;
		ResponseDTO response = null;

		try {
			Session session = sessionLogic.findBySessionId(sessionId);
			ResponseUtils.verifySession(session);
			ResponseUtils.verifySignature(signature, session.getSessionKey(), requestBody, false);

			// send data to proxy
			response = ewalletAuth.verifyOTP(requestBody);
			ResponseUtils.verifyProxyResponse(response);
			verifyResponse = new SignUpResponse(response);

			// Create response form
			debugger.logDebug("RESPONSE BODY FOR CLIENT", verifyResponse);
			return new ResponseEntity<SignUpResponse>(verifyResponse, Utils.getHttpHeaders(), HttpStatus.OK);
		} catch (Exception e) {
			debugger.errorException(e.getMessage(), e);
			return ResponseUtils.returnError(e.getMessage(), response);
		} 
	}

	@RequestMapping(value = "/sign_up", method = { RequestMethod.POST }, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> actionSignUp(@RequestBody String requestBody, HttpServletRequest req)
			throws IOException, InterruptedException {
		ResponseUtils.getRequestLog(req, requestBody, DEBUGGED_CLASS);
		String ip = Utils.getIP(req);
		String signature = req.getHeader("Signature");
		String sessionId = req.getHeader("Session-Id");
		String deviceId = req.getHeader(DEVICE_ID);
		String deviceDescript = req.getHeader(DEVICE_DESCRIPTION);
		SignUpResponse signUpResponse = null;
		ResponseDTO response = null;

		try {
			Session session = sessionLogic.findBySessionId(sessionId);
			ResponseUtils.verifySession(session);
			ResponseUtils.verifySignature(signature, session.getSessionKey(), requestBody, false);

			// send data to proxy
			response = ewalletAuth.signUp(requestBody, deviceId, ip,deviceDescript);
			ResponseUtils.verifyProxyResponse(response);
			signUpResponse = new SignUpResponse(response);

			// Create response form
			debugger.logDebug("RESPONSE BODY FOR CLIENT", Utils.masking2(signUpResponse));
			return new ResponseEntity<SignUpResponse>(signUpResponse, Utils.getHttpHeaders(), HttpStatus.OK);
		} catch (Exception e) {
			debugger.errorException(e.getMessage(), e);
			return ResponseUtils.returnError(e.getMessage(), response);
		}
	}

	@RequestMapping(value = "/sign_in", method = { RequestMethod.POST }, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> actionSignIn(@RequestBody String requestBody, HttpServletRequest req)
			throws IOException, InterruptedException {
		ResponseUtils.getRequestLog(req, requestBody, DEBUGGED_CLASS);
		String ip = Utils.getIP(req);
		String signature = req.getHeader("Signature");
		String sessionId = req.getHeader("Session-Id");
		String deviceId = req.getHeader(DEVICE_ID);
		SignInResponse signInResponse = null;
		ResponseDTO response = null;

		try {
			Session session = sessionLogic.findBySessionId(sessionId);
			ResponseUtils.verifySession(session);
			ResponseUtils.verifySignature(signature, session.getSessionKey(), requestBody, false);

			// send data to proxy
			response = ewalletAuth.signIn(requestBody, deviceId, ip);
			ResponseUtils.verifyProxyResponse(response);
			signInResponse = new SignInResponse(response);

			debugger.logDebug("RESPONSE BODY FOR CLIENT", Utils.masking2(signInResponse));
			return new ResponseEntity<SignInResponse>(signInResponse, Utils.getHttpHeaders(), HttpStatus.OK);
		} catch (Exception e) {
			debugger.errorException(e.getMessage(), e);
			return ResponseUtils.returnError(e.getMessage(), response);
		} 
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/sign_out", method = { RequestMethod.GET }, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> actionSignOut(HttpServletRequest req) throws IOException, InterruptedException {
		ResponseUtils.getRequestLog(req, "", DEBUGGED_CLASS);
		String signature = req.getHeader("Signature");
		String token = req.getHeader("Token");
		String sessionId = req.getHeader("Session-Id");
		ResponseDTO response = null;

		try {
			Session session = sessionLogic.findBySessionId(sessionId);
			ResponseUtils.verifySession(session);
			ResponseUtils.verifySignature(signature, session.getSessionKey(), "", false);

			// send data to proxy
			response = ewalletAuth.signOut(token);
			ResponseUtils.verifyProxyResponse(response);

			// Create response form
			return new ResponseEntity(Utils.getHttpHeaders(), HttpStatus.OK);
		} catch (Exception e) {
			debugger.errorException(e.getMessage(), e);
			return ResponseUtils.returnError(e.getMessage(), response);
		}
	}

	@RequestMapping(value = "/refresh_token", method = { RequestMethod.POST }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> actionRefreshToken(@RequestBody String requestBody, HttpServletRequest req) {
		ResponseUtils.getRequestLog(req,requestBody, DEBUGGED_CLASS);
		String ip = Utils.getIP(req);
		String signature = req.getHeader("Signature");
		String token = req.getHeader("Token");
		String sessionId = req.getHeader("Session-Id");
		String deviceId = req.getHeader(DEVICE_ID);
		RefreshTokenResponse refreshTokenResponse = null;
		ResponseDTO response = null;		
		try {
			
			Session session = sessionLogic.findBySessionId(sessionId);
			ResponseUtils.verifySession(session);
			ResponseUtils.verifySignature(signature, session.getSessionKey(),requestBody,false);
			
			// send data to proxy
			response = ewalletAuth.refreshToken(token, deviceId, ip,requestBody);
			ResponseUtils.verifyProxyResponse(response);
			refreshTokenResponse = new RefreshTokenResponse(response);
			
			// Create response form
			debugger.logDebug("RESPONSE BODY FOR CLIENT", Utils.masking2(refreshTokenResponse));
			return new ResponseEntity<RefreshTokenResponse>(refreshTokenResponse, Utils.getHttpHeaders(), HttpStatus.OK);
		} catch (Exception e) {
			debugger.errorException(e.getMessage(), e);
			return ResponseUtils.returnError(e.getMessage(), response);
		}		
	}

}
