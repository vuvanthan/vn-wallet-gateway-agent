package vn.truemoney.ewallet.gateway.api.rest;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import vn.truemoney.ewallet.gateway.businesslogic.session.ISessionLogic;
import vn.truemoney.ewallet.gateway.services.auth.IEWalletAuth;
import vn.truemoney.ewallet.gateway.services.billpay.IEWalletBillPay;
import vn.truemoney.framework.security.api.ICallerUtils;

public abstract class AbstractRestController implements InitializingBean{
	Gson gson = new Gson();
		
	protected static String DEVICE_ID = "Device-Id";
	
	protected static String DEVICE_DESCRIPTION = "Device-Description";
	
	protected static String REQUEST_ID = "Request-Id";
	
	protected static String SESSION_ID = "Session-Id";
	
	protected static String SIGNATURE = "Signature";
	
	
	@Autowired
	ICallerUtils callerUtils;
	// ------------------//

	@Autowired
	protected ISessionLogic sessionLogic;
	
	@Autowired
	protected IEWalletAuth ewalletAuth;
	
	@Autowired
	protected IEWalletBillPay ewalletBillPay;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}
}
