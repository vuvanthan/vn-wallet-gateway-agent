package vn.truemoney.ewallet.gateway.api.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vn.truemoney.ewallet.gateway.common.Debugger;
import vn.truemoney.ewallet.gateway.common.util.ResponseUtils;
import vn.truemoney.ewallet.gateway.common.util.Utils;
import vn.truemoney.ewallet.gateway.contract.billpay.BillPayDetailResponse;
import vn.truemoney.ewallet.gateway.contract.billpay.BillPayInfoResponse;
import vn.truemoney.ewallet.gateway.contract.billpay.OrderBillPayResponse;
import vn.truemoney.ewallet.gateway.contract.billpay.ServiceAndProviderResponse;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;
import vn.truemoney.ewallet.gateway.persistence.model.session.Session;

@RestController
@RequestMapping(value = "/api/mobile/v1")
public class BillPayRestController extends AbstractRestController {

	@SuppressWarnings("rawtypes")
	private final static Class DEBUGGED_CLASS = BillPayRestController.class;
	private Debugger debugger = new Debugger(DEBUGGED_CLASS);

	@RequestMapping(value = "/bill_pay/provider", method = { RequestMethod.GET }, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	
	public ResponseEntity<?> actionGetListServiceAndProvider(HttpServletRequest req) {
		ResponseUtils.getRequestLog(req, "", DEBUGGED_CLASS);
		String signature = req.getHeader("Signature");
		String token = req.getHeader("Token");
		String sessionId = req.getHeader("Session-Id");
		ServiceAndProviderResponse providerResponse = null;
		ResponseDTO response = null;

		try {
			Session session = sessionLogic.findBySessionId(sessionId);
			ResponseUtils.verifySession(session);
			ResponseUtils.verifySignature(signature, session.getSessionKey(), "", false);

			// send data for proxy
			response = ewalletBillPay.getListServiceAndProvider(token);
			ResponseUtils.verifyProxyResponse(response);
			providerResponse = new ServiceAndProviderResponse(response);

			debugger.logDebug("RESPONSE BODY FOR CLIENT", providerResponse);
			return new ResponseEntity<ServiceAndProviderResponse>(providerResponse, Utils.getHttpHeaders(),
					HttpStatus.OK);
		} catch (Exception e) {
			debugger.errorException(e.getMessage(), e);
			return ResponseUtils.returnError(e.getMessage(), response);
		}
	}

	@RequestMapping(value = "/bill_pay/info", method = { RequestMethod.POST }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> actionGetBillPayInfo(@RequestBody String requestBody, HttpServletRequest req) {
		ResponseUtils.getRequestLog(req, requestBody, DEBUGGED_CLASS);
		String signature = req.getHeader("Signature");
		String token = req.getHeader("Token");
		String sessionId = req.getHeader("Session-Id");
		BillPayInfoResponse billPayInfoResponse = null;
		ResponseDTO response = null;

		try {
			Session session = sessionLogic.findBySessionId(sessionId);
			ResponseUtils.verifySession(session);
			ResponseUtils.verifySignature(signature, session.getSessionKey(), requestBody, false);

			// send data for proxy
			response = ewalletBillPay.requestBillPayInfo(token, requestBody);
			ResponseUtils.verifyProxyResponse(response);
			billPayInfoResponse = new BillPayInfoResponse(response);

			debugger.logDebug("RESPONSE BODY FOR CLIENT", billPayInfoResponse);
			return new ResponseEntity<BillPayInfoResponse>(billPayInfoResponse, Utils.getHttpHeaders(), HttpStatus.OK);
		} catch (Exception e) {
			debugger.errorException(e.getMessage(), e);
			return ResponseUtils.returnError(e.getMessage(), response);
		}
	}

	@RequestMapping(value = "/bill_pay/order", method = { RequestMethod.POST }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> actionOrderBillPay(@RequestBody String requestBody, HttpServletRequest req) {
		ResponseUtils.getRequestLog(req, requestBody, DEBUGGED_CLASS);
		String signature = req.getHeader("Signature");
		String token = req.getHeader("Token");
		String sessionId = req.getHeader("Session-Id");
		OrderBillPayResponse orderBillPayResponse = null;
		ResponseDTO response = null;

		try {
			Session session = sessionLogic.findBySessionId(sessionId);
			ResponseUtils.verifySession(session);
			ResponseUtils.verifySignature(signature, session.getSessionKey(), requestBody, false);

			// send data for proxy
			response = ewalletBillPay.createOrderBillPay(token, requestBody);
			ResponseUtils.verifyProxyResponse(response);
			orderBillPayResponse = new OrderBillPayResponse(response);

			debugger.logDebug("RESPONSE BODY FOR CLIENT", Utils.masking2(orderBillPayResponse));
			return new ResponseEntity<OrderBillPayResponse>(orderBillPayResponse, Utils.getHttpHeaders(),
					HttpStatus.OK);
		} catch (Exception e) {
			debugger.errorException(e.getMessage(), e);
			return ResponseUtils.returnError(e.getMessage(), response);
		}
	}

	@RequestMapping(value = "/bill_pay/purchase", method = { RequestMethod.POST }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> actionConfirmBillPay(@RequestBody String requestBody, HttpServletRequest req) {
		ResponseUtils.getRequestLog(req, requestBody, DEBUGGED_CLASS);
		String signature = req.getHeader("Signature");
		String token = req.getHeader("Token");
		String sessionId = req.getHeader("Session-Id");
		BillPayDetailResponse billPayDetailResponse = null;
		ResponseDTO response = null;

		try {
			Session session = sessionLogic.findBySessionId(sessionId);
			ResponseUtils.verifySession(session);
			ResponseUtils.verifySignature(signature, session.getSessionKey(), requestBody, false);

			// send data for proxy
			response = ewalletBillPay.conformPurchase(token, requestBody);
			ResponseUtils.verifyProxyResponse(response);
			billPayDetailResponse = new BillPayDetailResponse(response);

			debugger.logDebug("RESPONSE BODY FOR CLIENT", billPayDetailResponse);
			return new ResponseEntity<BillPayDetailResponse>(billPayDetailResponse, Utils.getHttpHeaders(),
					HttpStatus.OK);
		} catch (Exception e) {
			debugger.errorException(e.getMessage(), e);
			return ResponseUtils.returnError(e.getMessage(), response);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/bill_pay/resend_otp", method = { RequestMethod.POST }, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> actionResendOTP(@RequestBody String requestBody, HttpServletRequest req) {
		ResponseUtils.getRequestLog(req, requestBody, DEBUGGED_CLASS);
		String signature = req.getHeader("Signature");
		String token = req.getHeader("Token");
		String sessionId = req.getHeader("Session-Id");
		ResponseDTO response = null;

		try {
			Session session = sessionLogic.findBySessionId(sessionId);
			ResponseUtils.verifySession(session);
			ResponseUtils.verifySignature(signature, session.getSessionKey(), requestBody, false);

			// send data for proxy
			response = ewalletBillPay.resendOTP(token, requestBody);
			ResponseUtils.verifyProxyResponse(response);

			debugger.logDebug("RESPONSE BODY FOR CLIENT", response);
			return new ResponseEntity(Utils.getHttpHeaders(), HttpStatus.OK);
		} catch (Exception e) {
			debugger.errorException(e.getMessage(), e);
			return ResponseUtils.returnError(e.getMessage(), response);
		}
	}
}
