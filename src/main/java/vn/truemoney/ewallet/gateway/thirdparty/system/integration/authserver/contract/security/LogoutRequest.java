package vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.base.RequestType;

public class LogoutRequest extends RequestType implements Serializable {
  private String test = "Bye bye Server";

  public String getTest() {
    return test;
  }

  public void setTest(String test) {
    this.test = test;
  }
}
