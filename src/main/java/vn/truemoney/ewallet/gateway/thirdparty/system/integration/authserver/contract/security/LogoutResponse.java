package vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.base.ResponseType;

public class LogoutResponse extends ResponseType implements Serializable {
  protected boolean signedOut;

  public boolean isSignedOut() {
    return signedOut;
  }

  public void setSignedOut(boolean signedOut) {
    this.signedOut = signedOut;
  }

}
