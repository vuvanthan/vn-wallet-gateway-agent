package vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.base.ResponseType;

public class ResetPasswordResponse extends ResponseType implements Serializable {
  protected boolean updated;

  public boolean isUpdated() {
    return updated;
  }

  public void setUpdated(boolean updated) {
    this.updated = updated;
  }

}
