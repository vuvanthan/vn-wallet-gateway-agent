package vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.base.RequestType;
import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security.bean.Role;

public class CreateRoleRequest extends RequestType implements Serializable {

  protected Role role;

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

}
