package vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.base.RequestType;

public class FindUserRequest extends RequestType implements Serializable {

  protected String username;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }
}
