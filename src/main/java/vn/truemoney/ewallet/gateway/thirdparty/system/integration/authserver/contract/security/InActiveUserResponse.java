package vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.base.ResponseType;

public class InActiveUserResponse extends ResponseType implements Serializable {
  protected boolean inactive;

  public boolean isInactive() {
    return inactive;
  }

  public void setInactive(boolean inactive) {
    this.inactive = inactive;
  }
}
