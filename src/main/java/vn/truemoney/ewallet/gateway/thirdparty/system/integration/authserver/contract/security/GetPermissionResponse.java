package vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.base.ResponseType;
import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security.bean.Permission;

public class GetPermissionResponse extends ResponseType implements Serializable {
  protected Permission permission;

  public Permission getPermission() {
    return permission;
  }

  public void setPermission(Permission permission) {
    this.permission = permission;
  }
}
