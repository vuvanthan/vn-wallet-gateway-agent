package vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.base.ResponseType;

public class UpdateRoleResponse extends ResponseType implements Serializable {
  protected Long roleId;

  public Long getRoleId() {
    return roleId;
  }

  public void setRoleId(Long roleId) {
    this.roleId = roleId;
  }

}
