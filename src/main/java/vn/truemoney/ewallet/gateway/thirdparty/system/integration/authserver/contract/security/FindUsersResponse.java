package vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;
import java.util.Collection;

import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.base.ResponseType;
import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security.bean.User;

public class FindUsersResponse extends ResponseType implements Serializable {
  protected Collection<User> users;
  protected Long total;

  public Collection<User> getUsers() {
    return users;
  }

  public void setUsers(Collection<User> users) {
    this.users = users;
  }

  public Long getTotal() {
    return total;
  }

  public void setTotal(Long total) {
    this.total = total;
  }
}
