package vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security;

import java.io.Serializable;

import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.base.ResponseType;
import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security.bean.User;

public class FindRoleResponse extends ResponseType implements Serializable {
  protected User user;

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
