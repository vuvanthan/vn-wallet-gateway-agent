package vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver;

import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security.CreateUserRequest;
import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security.CreateUserResponse;
import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security.GetUserRequest;
import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security.GetUserResponse;
import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security.UpdateUserRequest;
import vn.truemoney.ewallet.gateway.thirdparty.system.integration.authserver.contract.security.UpdateUserResponse;

public interface IAuthService {
  void setAccessToken(String accessToken);

  public GetUserResponse getUser(GetUserRequest request) throws Exception;

  CreateUserResponse createUser(CreateUserRequest request) throws Exception;

  UpdateUserResponse updateUser(UpdateUserRequest request) throws Exception;

}
