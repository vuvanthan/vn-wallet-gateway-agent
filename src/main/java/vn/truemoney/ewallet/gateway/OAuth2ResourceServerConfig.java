package vn.truemoney.ewallet.gateway;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

import com.google.gson.Gson;

@Configuration
//@PropertySource({ "classpath:persistence.properties" })
@EnableResourceServer
public class OAuth2ResourceServerConfig extends ResourceServerConfigurerAdapter {

    //@Autowired
    //private Environment env;
	
	@Value("${application.integration.oauth-base-url}")
    private String OAUTH_URL;
	
	@Value("${application.integration.oauth-client-id}")
    private String CLIENT_ID;
	
	@Value("${application.integration.oauth-secret}")
    private String SECRET;
	
	@Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.tokenExtractor(new CustomTokenExtractor());
    }
	
    @Override
    public void configure(final HttpSecurity http) throws Exception {
        // @formatter:off
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).and().authorizeRequests()
				.anyRequest().permitAll();
		// @formatter:on
    }

    // Remote token service
    @Primary
    @Bean
    public RemoteTokenServices tokenService() {
        final RemoteTokenServices tokenService = new RemoteTokenServices();
        
        tokenService.setCheckTokenEndpointUrl(OAUTH_URL);
        tokenService.setClientId(CLIENT_ID);
        tokenService.setClientSecret(SECRET);
        tokenService.setAccessTokenConverter(accessTokenConverter());
        return tokenService;
    }
    
    @Bean
	public AccessTokenConverter accessTokenConverter() {
    	DefaultAccessTokenConverter tokenConverter = new DefaultAccessTokenConverter();
    	tokenConverter.setUserTokenConverter(new DefaultUserAuthenticationConverter() {

			@Override
    	    public Authentication extractAuthentication(Map<String, ?> map) {
    	        Authentication authentication = super.extractAuthentication(map);
//    	        System.out.println(new Gson().toJson(map, Map.class));
    	        // User is my custom UserDetails class
    	        UserAccount userAccount = new UserAccount();
    	        userAccount.setAuthority(authentication.getAuthorities());
    	        userAccount.setAccountId(String.valueOf(map.get("cus_id")));
    	        userAccount.setEqId(String.valueOf(map.get("ref_cus_id")));
    	        
//    	        Collection oldAuthorities = authentication.getAuthorities();
//    	        
//    	        Set<GrantedAuthority> pers = new HashSet<>();
//				GrantedAuthority per = new SimpleGrantedAuthority("ROLE_USER");
//				pers.add(per);
    	        return new UsernamePasswordAuthenticationToken(userAccount,
    	                authentication.getCredentials(), authentication.getAuthorities());
    	    }
    	});
		return tokenConverter;
	}
    
/*    @Bean
    public AuthenticationManager authenticationManager() {
        final MyOAuth2AuthenticationManager oAuth2AuthenticationManager = new MyOAuth2AuthenticationManager();
        oAuth2AuthenticationManager.setTokenServices(tokenService());
        return oAuth2AuthenticationManager;
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.tokenServices(tokenService()).authenticationManager(authenticationManager());
    }*/
    
    // JWT token store

   /* @Override
    public void configure(final ResourceServerSecurityConfigurer config) {
        config.tokenServices(tokenService());
    }*/

    
    /*
    @Bean
    public TokenStore tokenStore() {
    return new JwtTokenStore(accessTokenConverter());
    }
    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
    final JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
    // converter.setSigningKey("123");
    final Resource resource = new ClassPathResource("public.txt");
    String publicKey = null;
    try {
        publicKey = IOUtils.toString(resource.getInputStream());
    } catch (final IOException e) {
        throw new RuntimeException(e);
    }
    converter.setVerifierKey(publicKey);
    return converter;
    }
    */
    /*@Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        return defaultTokenServices;
    }*/

    // JDBC token store configuration

    /*@Bean
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
        dataSource.setUrl(env.getProperty("jdbc.url"));
        dataSource.setUsername(env.getProperty("jdbc.user"));
        dataSource.setPassword(env.getProperty("jdbc.pass"));
        return dataSource;
    }*/

   /* @Bean
    public TokenStore tokenStore() {
        return new JdbcTokenStore(dataSource());
    }*/

}
