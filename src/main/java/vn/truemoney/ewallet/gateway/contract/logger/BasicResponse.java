package vn.truemoney.ewallet.gateway.contract.logger;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BasicResponse {

	private String code;
	private String msg;
	@JsonIgnore
	private String lang = "vi";

	public BasicResponse() {
	}

	public BasicResponse(String code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}
	
	public BasicResponse(String code, String msg, String lang) {
		super();
		this.code = code;
		this.msg = msg;
		this.lang = lang;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
		
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

}
