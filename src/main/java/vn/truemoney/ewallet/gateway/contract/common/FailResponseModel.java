package vn.truemoney.ewallet.gateway.contract.common;

import java.io.Serializable;

@SuppressWarnings("serial")
public class FailResponseModel implements Serializable {

	private String code;
	private String msg;
	private Object response;

	public FailResponseModel(String code, String msg, Object response) {
		this.code = code;
		this.msg = msg;
		this.response = response;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}
}
