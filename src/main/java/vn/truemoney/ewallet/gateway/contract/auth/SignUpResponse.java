package vn.truemoney.ewallet.gateway.contract.auth;

import java.io.Serializable;
import java.util.HashMap;

import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;

@SuppressWarnings("serial")
public class SignUpResponse implements Serializable {
	private String token;

	private String verify_token;

	private String security_code;

	private String refresh_token;

	private String expires_in;

	public SignUpResponse(ResponseDTO signUpResponse) {
		if (signUpResponse.getData() != null) {
			HashMap<String, Object> data = signUpResponse.getData();

			if (data.containsKey("access_token") && data.get("access_token") != null) {
				this.token = data.get("access_token").toString();
			}

			if (data.containsKey("verify_token") && data.get("verify_token") != null) {
				this.verify_token = data.get("verify_token").toString();
			}

			if (data.containsKey("security_code") && data.get("security_code") != null) {
				this.security_code = data.get("security_code").toString();
			}
			
			if (data.containsKey("refresh_token") && data.get("refresh_token") != null) {
				this.refresh_token = data.get("refresh_token").toString();
			}
			
			if (data.containsKey("expires_in") && data.get("expires_in") != null) {
				this.expires_in = data.get("expires_in").toString();
			}
		} else {
			this.token = null;
		}

	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getVerify_token() {
		return verify_token;
	}

	public void setVerify_token(String verify_token) {
		this.verify_token = verify_token;
	}

	public String getSecurity_code() {
		return security_code;
	}

	public void setSecurity_code(String security_code) {
		this.security_code = security_code;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	public String getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(String expires_in) {
		this.expires_in = expires_in;
	}
}
