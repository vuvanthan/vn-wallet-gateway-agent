package vn.truemoney.ewallet.gateway.contract.auth;

import java.util.HashMap;

import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;

public class RefreshTokenResponse {

	private String access_token;
	private String token_type;
	private String expires_in;
	private String refresh_token;
	private String refresh_token_expires_in;
	private String correlation_id;
	
	public RefreshTokenResponse(ResponseDTO tokenResponse) {
		if (tokenResponse.getData() != null) {
			HashMap<String, Object> data = tokenResponse.getData();

			if (data.containsKey("access_token") && data.get("access_token") != null) {
				this.access_token = data.get("access_token").toString();
			}
			
			if (data.containsKey("token_type") && data.get("token_type") != null) {
				this.token_type = data.get("token_type").toString();
			}
			
			if (data.containsKey("expires_in") && data.get("expires_in") != null) {
				this.expires_in = data.get("expires_in").toString();
			}

			if (data.containsKey("refresh_token") && data.get("refresh_token") != null) {
				this.refresh_token = data.get("refresh_token").toString();
			}
			
			if (data.containsKey("refresh_token_expires_in") && data.get("refresh_token_expires_in") != null) {
				this.refresh_token_expires_in = data.get("refresh_token_expires_in").toString();
			}
			
			if (data.containsKey("correlation_id") && data.get("correlation_id") != null) {
				this.correlation_id = data.get("correlation_id").toString();
			}
		}
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public String getToken_type() {
		return token_type;
	}

	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}

	public String getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(String expires_in) {
		this.expires_in = expires_in;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	public String getRefresh_token_expires_in() {
		return refresh_token_expires_in;
	}

	public void setRefresh_token_expires_in(String refresh_token_expires_in) {
		this.refresh_token_expires_in = refresh_token_expires_in;
	}

	public String getCorrelation_id() {
		return correlation_id;
	}

	public void setCorrelation_id(String correlation_id) {
		this.correlation_id = correlation_id;
	}
	
	
}
