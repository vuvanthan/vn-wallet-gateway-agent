package vn.truemoney.ewallet.gateway.contract.auth;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SignInRequest implements Serializable {
	private String phone_number;

	private String pin;

	// kyc version 2
	private String device_id;

	private String public_ip;

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getDevice_id() {
		return device_id;
	}

	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}

	public String getPublic_ip() {
		return public_ip;
	}

	public void setPublic_ip(String public_ip) {
		this.public_ip = public_ip;
	}
}
