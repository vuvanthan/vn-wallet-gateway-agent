package vn.truemoney.ewallet.gateway.contract.session;

import java.io.Serializable;

public class SessionRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	private String app_version;

	public String getApp_version() {
		return app_version;
	}

	public void setApp_version(String app_version) {
		this.app_version = app_version;
	}
}
