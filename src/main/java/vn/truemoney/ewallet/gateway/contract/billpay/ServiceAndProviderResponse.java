package vn.truemoney.ewallet.gateway.contract.billpay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;

public class ServiceAndProviderResponse {
	
	private static final String IMAGE_PATH = "/images/billpay/";
	private static final String IMAGE_TYPE = ".png";
	private List<Object> services;
	
	@SuppressWarnings("unchecked")
	public ServiceAndProviderResponse(ResponseDTO serviceAndProviderResponse) {
		
		HashMap<String, Object> data = serviceAndProviderResponse.getData();
		
		if(data.containsKey("services") && data.get("services") != null) {
			List<Object> serviceList = new ArrayList<>();
			List<Object> serviceListData = (List<Object>) data.get("services");
			
			for(int i = 0; i < serviceListData.size(); i++) {
				HashMap<String,Object> serviceObj = (HashMap<String, Object>) serviceListData.get(i);
				HashMap<String,Object> service = new HashMap<String, Object>();
				
				if(serviceObj.containsKey("name_vi") && serviceObj.get("name_vi") != null) {
					service.put("name_vi",serviceObj.get("name_vi"));
				
				}
				if(serviceObj.containsKey("name_en") && serviceObj.get("name_en") != null) {
					service.put("name_en",serviceObj.get("name_en"));
				}
				
				if(serviceObj.containsKey("service_code") && serviceObj.get("service_code") != null) {
					service.put("service_code",serviceObj.get("service_code").toString());
				}
				if(serviceObj.containsKey("limit_min") && serviceObj.get("limit_min") != null) {
					service.put("limit_min",serviceObj.get("limit_min").toString());
				}
				
				if(serviceObj.containsKey("limit_max") && serviceObj.get("limit_max") != null) {
					service.put("limit_max",serviceObj.get("limit_max").toString());
				}
				
				List<Object> childs = new ArrayList<>();
				List<Object> providerListData = (List<Object>) serviceObj.get("childs");
				
				for(int j = 0; j < providerListData.size(); j++) {
					HashMap<String,Object> providerObj = (HashMap<String, Object>) providerListData.get(j);
					HashMap<String,Object> provider = new HashMap<String, Object>();
					
					if(providerObj.containsKey("name_vi") && providerObj.get("name_vi") != null) {
						provider.put("name_vi",providerObj.get("name_vi").toString());	
					}
					if(providerObj.containsKey("name_en") && providerObj.get("name_en") != null) {
						provider.put("name_en",providerObj.get("name_en").toString());
					}
					
					if(providerObj.containsKey("service_code") && providerObj.get("service_code") != null) {
						provider.put("service_code",providerObj.get("service_code").toString());
						provider.put("path", IMAGE_PATH  + providerObj.get("service_code").toString() + IMAGE_TYPE);
						
					}
					
					if (providerObj.containsKey("min_limit") && providerObj.get("min_limit") != null) {
						provider.put("limit_min", providerObj.get("min_limit").toString());
					}
					
					if (providerObj.containsKey("max_limit") && providerObj.get("max_limit") != null) {
						provider.put("limit_max", providerObj.get("max_limit").toString());
					}
					
					List<Object> childs2 = new ArrayList<>();
					List<Object> providerListData2 = (List<Object>) providerObj.get("childs");
					
					for(int k = 0; k < providerListData2.size(); k++) {
						HashMap<String,Object> providerObj2 = (HashMap<String, Object>) providerListData2.get(k);
						HashMap<String,Object> provider2 = new HashMap<String, Object>();
						
						if(providerObj2.containsKey("name_vi") && providerObj2.get("name_vi") != null) {
							provider2.put("name_vi",providerObj2.get("name_vi").toString());	
						}
						if(providerObj2.containsKey("name_en") && providerObj2.get("name_en") != null) {
							provider2.put("name_en",providerObj2.get("name_en").toString());
						}
						
						if(providerObj2.containsKey("service_code") && providerObj2.get("service_code") != null) {
							provider2.put("service_code",providerObj2.get("service_code").toString());
							provider2.put("path", IMAGE_PATH + "provider/" + providerObj2.get("service_code").toString() + IMAGE_TYPE);
							provider2.put("path_description", IMAGE_PATH  + "description/" + providerObj2.get("service_code").toString() + IMAGE_TYPE);
						}
						
						if (providerObj2.containsKey("min_limit") && providerObj2.get("min_limit") != null) {
							provider2.put("limit_min", providerObj2.get("min_limit").toString());
						}
						
						if (providerObj2.containsKey("max_limit") && providerObj2.get("max_limit") != null) {
							provider2.put("limit_max", providerObj2.get("max_limit").toString());
						}
						
						if (providerObj2.containsKey("service_name_vi") && providerObj2.get("service_name_vi") != null) {
							provider2.put("service_name_vi", providerObj2.get("service_name_vi").toString());
						}
						
						if (providerObj2.containsKey("service_name_en") && providerObj2.get("service_name_en") != null) {
							provider2.put("service_name_en", providerObj2.get("service_name_en").toString());
						}
						
						childs2.add(provider2);
					}
					provider.put("childs",childs2);
					childs.add(provider);
				
				}
				if(childs.size() > 0) {
					service.put("childs",childs);
				}
				serviceList.add(service);
			}			
			this.services = serviceList;
		}
	}

	public List<Object> getServices() {
		return services;
	}

	public void setServices(List<Object> services) {
		this.services = services;
	}
}
