package vn.truemoney.ewallet.gateway.contract.billpay;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;

import vn.truemoney.ewallet.gateway.common.util.Utils;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;

public class OrderBillPayResponse {

	@JsonProperty("verify_token")
	private String verifyToken;

	@JsonProperty("customer_number")
	private String customerNumber;

	@JsonProperty("full_name")
	private String fullName;

	@JsonProperty("order_id")
	private String orderId;

	@JsonProperty("amount")
	private Long amount;

	@JsonProperty("fee")
	private Long fee;

	@JsonProperty("discount")
	private Long discount;

	@JsonProperty("total_amount")
	private Long totalAmount;

	public OrderBillPayResponse(ResponseDTO orderBillPayResponse) {
		HashMap<String, Object> data = orderBillPayResponse.getData();

		if (data.containsKey("verify_token") && data.get("verify_token") != null) {
			this.verifyToken = data.get("verify_token").toString();

		}
		if (data.containsKey("customer_number") && data.get("customer_number") != null) {
			this.customerNumber = data.get("customer_number").toString();

		}

		if (data.containsKey("full_name") && data.get("full_name") != null) {
			this.fullName = data.get("full_name").toString();

		}

		if (data.containsKey("order_id") && data.get("order_id") != null) {
			this.orderId = data.get("order_id").toString();

		}

		if (data.containsKey("amount") && data.get("amount") != null) {
			this.amount = Utils.getRoundNumber(data.get("amount").toString());
		}

		if (data.containsKey("fee") && data.get("fee") != null) {
			this.fee = Utils.getRoundNumber(data.get("fee").toString());
		}

		if (data.containsKey("discount") && data.get("discount") != null) {
			this.discount = Utils.getRoundNumber(data.get("discount").toString());
		}

		if (data.containsKey("total_amount") && data.get("total_amount") != null) {
			this.totalAmount = Utils.getRoundNumber(data.get("total_amount").toString());
		}
	}

	public String getVerifyToken() {
		return verifyToken;
	}

	public void setVerifyToken(String verifyToken) {
		this.verifyToken = verifyToken;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getFee() {
		return fee;
	}

	public void setFee(Long fee) {
		this.fee = fee;
	}

	public Long getDiscount() {
		return discount;
	}

	public void setDiscount(Long discount) {
		this.discount = discount;
	}

	public Long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Long totalAmount) {
		this.totalAmount = totalAmount;
	}

}
