package vn.truemoney.ewallet.gateway.contract.logger;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author linhnm
 * 
 */
public class LoggerLevelRequest {
	@JsonProperty("configured_level")
	private String configuredLevel;

	@JsonProperty("package_name")
	private String packageName;

	public String getConfiguredLevel() {
		return configuredLevel;
	}

	public void setConfiguredLevel(String configuredLevel) {
		this.configuredLevel = configuredLevel;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

}