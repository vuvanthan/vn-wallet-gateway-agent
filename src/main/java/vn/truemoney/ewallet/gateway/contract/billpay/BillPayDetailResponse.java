package vn.truemoney.ewallet.gateway.contract.billpay;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;

import vn.truemoney.ewallet.gateway.common.util.Utils;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;

public class BillPayDetailResponse {

	@JsonProperty("amount")
	private Long amount;

	@JsonProperty("customer_number")
	private String customerNumber;

	@JsonProperty("full_name")
	private String fullName;

	@JsonProperty("fee")
	private Long fee;

	@JsonProperty("discount")
	private Long discount;

	@JsonProperty("total_amount")
	private Long totalAmount;

	@JsonProperty("transaction_id")
	private String transactionId;

	@JsonProperty("transaction_time")
	private String transactionTime;

	@JsonProperty("pre_balance")
	private String preBalance;

	@JsonProperty("post_balance")
	private String postBalance;

	public BillPayDetailResponse(ResponseDTO billPayDetailResponse) {
		HashMap<String, Object> data = billPayDetailResponse.getData();

		if (data.containsKey("amount") && data.get("amount") != null) {

			this.amount = Utils.getRoundNumber(data.get("amount").toString());
		}

		if (data.containsKey("fee") && data.get("fee") != null) {

			this.fee = Utils.getRoundNumber(data.get("fee").toString());
		}

		if (data.containsKey("customer_number") && data.get("customer_number") != null) {

			this.customerNumber = data.get("customer_number").toString();
		}

		if (data.containsKey("full_name") && data.get("full_name") != null) {

			this.fullName = data.get("fee").toString();
		}

		if (data.containsKey("discount") && data.get("discount") != null) {

			this.discount = Utils.getRoundNumber(data.get("discount").toString());
		}
		if (data.containsKey("total_amount") && data.get("total_amount") != null) {

			this.totalAmount = Utils.getRoundNumber(data.get("total_amount").toString());
		}
		if (data.containsKey("transaction_id") && data.get("transaction_id") != null) {

			this.transactionId = data.get("transaction_id").toString();
		}

		if (data.containsKey("transaction_time") && data.get("transaction_time") != null) {

			this.transactionTime = data.get("transaction_time").toString();
		}

		if (data.containsKey("pre_balance") && data.get("pre_balance") != null) {

			this.preBalance = data.get("pre_balance").toString();
		}

		if (data.containsKey("post_balance") && data.get("post_balance") != null) {

			this.postBalance = data.get("post_balance").toString();
		}

	}

	public Long getFee() {
		return fee;
	}

	public void setFee(Long fee) {
		this.fee = fee;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Long getDiscount() {
		return discount;
	}

	public void setDiscount(Long discount) {
		this.discount = discount;
	}

	public Long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Long totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(String transactionTime) {
		this.transactionTime = transactionTime;
	}

	public String getPreBalance() {
		return preBalance;
	}

	public void setPreBalance(String preBalance) {
		this.preBalance = preBalance;
	}

	public String getPostBalance() {
		return postBalance;
	}

	public void setPostBalance(String postBalance) {
		this.postBalance = postBalance;
	}

}
