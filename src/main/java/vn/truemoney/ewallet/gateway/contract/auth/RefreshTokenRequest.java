package vn.truemoney.ewallet.gateway.contract.auth;

public class RefreshTokenRequest {
	
	private String refresh_token;
	private String device_id;
	private String public_ip;
	
	
	public String getRefresh_token() {
		return refresh_token;
	}
	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public String getPublic_ip() {
		return public_ip;
	}
	public void setPublic_ip(String public_ip) {
		this.public_ip = public_ip;
	}
	
	
}
