package vn.truemoney.ewallet.gateway.contract.logger;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BasicRequest {
	@JsonProperty("notification_id")
	private String notificationId;

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}
}
