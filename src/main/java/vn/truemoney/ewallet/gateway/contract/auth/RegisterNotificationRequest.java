package vn.truemoney.ewallet.gateway.contract.auth;

public class RegisterNotificationRequest {

	private String phone_number;
	private String device_token;
	
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public String getDevice_token() {
		return device_token;
	}
	public void setDevice_token(String device_token) {
		this.device_token = device_token;
	}
		
}
