package vn.truemoney.ewallet.gateway.contract.billpay;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;

import vn.truemoney.ewallet.gateway.common.util.Utils;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;

public class BillPayInfoResponse {

	private static final String IMAGE_PATH = "/images/billpay/";
	private static final String IMAGE_TYPE = ".png";

	@JsonProperty("service_code")
	private String serviceCode;

	private String info;

	private String address;

	private String period;

	@JsonProperty("citizen_id")
	private String citizenId;

	@JsonProperty("payment_at_least")
	private Long paymentAtLeast;

	@JsonProperty("payment_code")
	private String paymentCode;

	@JsonProperty("contract_no")
	private String contractNo;

	@JsonProperty("package_name")
	private String packageName;

	@JsonProperty("due_date")
	private String dueDate;

	@JsonProperty("debt")
	private Long debt;

	@JsonProperty("path")
	private String path;

	@JsonProperty("customer_name")
	private String customerName;

	@JsonProperty("expired_date")
	private String expriedDate;
	
	@JsonProperty("duration")
	private String duration;

	public BillPayInfoResponse(ResponseDTO billPayInfoResponse) {

		HashMap<String, Object> data = billPayInfoResponse.getData();

		if (data.containsKey("service_code") && data.get("service_code") != null) {
			this.serviceCode = data.get("service_code").toString();
			this.path = IMAGE_PATH + "provider/" + data.get("service_code").toString() + IMAGE_TYPE;
		}

		if (data.containsKey("debt") && data.get("debt") != null) {
			this.debt = Utils.getRoundNumber(data.get("debt").toString());
		}

		if (data.containsKey("customer_name") && data.get("customer_name") != null) {
			this.customerName = data.get("customer_name").toString();
		}

		if (data.containsKey("expired_date") && data.get("expired_date") != null) {
			this.expriedDate = data.get("expired_date").toString();
		}

		if (data.containsKey("info") && data.get("info") != null) {
			this.info = data.get("info").toString();
		}

		if (data.containsKey("address") && data.get("address") != null) {
			this.address = data.get("address").toString();
		}

		if (data.containsKey("period") && data.get("period") != null) {
			this.period = data.get("period").toString();
		}

		if (data.containsKey("citizen_id") && data.get("citizen_id") != null) {
			this.citizenId = data.get("citizen_id").toString();
		}

		if (data.containsKey("payment_at_least") && data.get("payment_at_least") != null) {
			this.paymentAtLeast = Utils.getRoundNumber(data.get("payment_at_least").toString());
		}

		if (data.containsKey("payment_code") && data.get("payment_code") != null) {
			this.paymentCode = data.get("payment_code").toString();
		}

		if (data.containsKey("contract_no") && data.get("contract_no") != null) {
			this.contractNo = data.get("contract_no").toString();
		}

		if (data.containsKey("package_name") && data.get("package_name") != null) {
			this.packageName = data.get("package_name").toString();
		}
		
		if (data.containsKey("due_date") && data.get("due_date") != null) {
			this.dueDate = data.get("due_date").toString();
		}
		
		if (data.containsKey("duration") && data.get("duration") != null) {
			this.duration = data.get("duration").toString();
		}
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getCitizenId() {
		return citizenId;
	}

	public void setCitizenId(String citizenId) {
		this.citizenId = citizenId;
	}

	public Long getPaymentAtLeast() {
		return paymentAtLeast;
	}

	public void setPaymentAtLeast(Long paymentAtLeast) {
		this.paymentAtLeast = paymentAtLeast;
	}

	public String getPaymentCode() {
		return paymentCode;
	}

	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public Long getDebt() {
		return debt;
	}

	public void setDebt(Long debt) {
		this.debt = debt;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getExpriedDate() {
		return expriedDate;
	}

	public void setExpriedDate(String expriedDate) {
		this.expriedDate = expriedDate;
	}
	
	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

}
