package vn.truemoney.ewallet.gateway.contract.session;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("serial")
public class SessionResponse implements Serializable { 
	@JsonProperty("session_id")
	private String sessionId;
	
	@JsonProperty("session_key")
	private String sessionKey;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
}
