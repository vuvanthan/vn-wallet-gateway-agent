package vn.truemoney.ewallet.gateway;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SharedConstants {

	public static String PRIVATE_KEY = "";

	@Value("${application.private-key}")
	public void setPrivateKey(String value) {
		PRIVATE_KEY = value;
	}

	// SOME CHARACTERS
	public static final String SYS_NAME = "MYTOPUP";
	public static final String COMMA = ",";
	public static final String COLON = ":";
	public static final String DASH = "-";
	public static final String SEMI_CONLON = ";";
	public static final String VERTICAL_BAR = "|";
	public static final String UNDERSCORE = "_";

	public static String AUTH_SERVER_BASE_REQUEST_URL = "";

	// Email
	public static String EMAIL_FROM = "";

	public static class Role {

		public static final String ROLE_SYSTEM = "ROLE_SYSTEM";

		public static final String ROLE_CUSTOMER = "ROLE_CUSTOMER";

		public static final String ROLE_USER = "ROLE_USER";

		public static final String ROLE_MERCHANT = "ROLE_MERCHANT";

		public static final String ROLE_STAFF = "ROLE_STAFF";

		public static final String ROLE_ADMIN = "ROLE_ADMIN";

		public static final String ROLE_FINANCE = "ROLE_FINANCE";

		public static final String ROLE_SALE = "ROLE_SALE";

		public static final String ROLE_SALE_MANAGER = "ROLE_SALE_MANAGER";

		public static final String ROLE_SALE_SUPPORT = "ROLE_SALE_SUPPORT";

		public static final String ROLE_CUSTOMER_CARE = "ROLE_CUSTOMER_CARE";

	}

	public static int HTTP_CLIENT_CONNECT_TIMEOUT = 5000;// ms
	public static int HTTP_CLIENT_SOCKET_TIMEOUT = 20000;// ms

	public static int SLA_AIRTIME_TIMEOUT = 3000000;// ms
	public static int SLA_PROVIDER_TIMEOUT = HTTP_CLIENT_SOCKET_TIMEOUT + 2000;// ms
}
