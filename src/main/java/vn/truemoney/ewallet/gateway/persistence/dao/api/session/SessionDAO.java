package vn.truemoney.ewallet.gateway.persistence.dao.api.session;

import java.util.Date;

import vn.truemoney.ewallet.gateway.persistence.model.session.Session;
import vn.truemoney.framework.persistence.dao.GeneratedIdDAO;

public interface SessionDAO extends GeneratedIdDAO<Session> {
	public Session findBySessionId(String sessionId);
	
	public int inActiveSession(Date expiredTime);
}
