package vn.truemoney.ewallet.gateway.persistence.dao.hibernate.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import vn.truemoney.ewallet.gateway.persistence.dao.api.factory.DaoFactory;
import vn.truemoney.ewallet.gateway.persistence.dao.api.session.SessionDAO;
import vn.truemoney.ewallet.gateway.persistence.dao.api.transaction.TransactionLogDAO;

@Repository
public class HibernateDAOFactory implements DaoFactory {	
	@Autowired
	private SessionDAO sessionDAO;
	
	@Autowired
	private TransactionLogDAO transactionLogDAO;

	@Override
	public SessionDAO getSessionDAO() {
		return sessionDAO;
	}

	@Override
	public TransactionLogDAO getTransactionLogDAO() {
		return transactionLogDAO;
	}

}
