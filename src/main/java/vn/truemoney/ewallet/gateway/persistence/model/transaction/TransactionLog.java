package vn.truemoney.ewallet.gateway.persistence.model.transaction;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import vn.truemoney.framework.persistence.model.GeneratedIdEntry;


@Entity
@Table(name = "VNWALLETGW_TRANSACTION_LOG")
@AttributeOverride(name = "id", column = @Column(name = "ID"))
public class TransactionLog extends GeneratedIdEntry implements Cloneable {
    private static final long serialVersionUID = 1L;

    @Column(name = "SESSION_ID", nullable = false, length = 16)
    private String sessionId;

    @Column(name = "REQUEST_ID", nullable = false, length = 36)
    private String requestId;
    
    @Column(name = "IP", nullable = false, length = 16)
    private String ip;
    
    @Column(name = "REQUEST_URI", nullable = false, length = 100)
    private String requestURI;
    
    @Column(name = "BODY", nullable = true)
    private String body;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "REQUEST_TIME", nullable = true)
    private Date requestTime;
    
    @Column(name = "METHOD", nullable = true)
    private String method;
    
    @Column(name = "TYPEGW", nullable = true)
    private long typegw;
    
    @Column(name = "PARENT", nullable = true)
    private Long parent;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RESPONSE_TIME", nullable = true)
    private Date responseTime;
    
    

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getRequestURI() {
		return requestURI;
	}

	public void setRequestURI(String requestURI) {
		this.requestURI = requestURI;
	}	
	
	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Date getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
	public long getTypegw() {
		return typegw;
	}

	public void setTypegw(long typegw) {
		this.typegw = typegw;
	}
	
	public Long getParent() {
		return parent;
	}

	public void setParent(Long parent) {
		this.parent = parent;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Date responseTime) {
		this.responseTime = responseTime;
	}
	
	public TransactionLog clone() throws CloneNotSupportedException{  
		return (TransactionLog)super.clone();  
	}  
}
