package vn.truemoney.ewallet.gateway.persistence.model.session;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import vn.truemoney.framework.persistence.model.GeneratedIdEntry;


@Entity
@Table(name = "VNWALLETGW_SESSION")
@AttributeOverride(name = "id", column = @Column(name = "ID"))
public class Session extends GeneratedIdEntry {
    private static final long serialVersionUID = 1L;

    @Column(name = "SESSION_ID", nullable = false, length = 16)
    private String sessionId;
    
    @Column(name = "SESSION_KEY", nullable = false, length = 32)
    private String sessionKey;

    @Column(name = "REQUEST_ID", nullable = false, length = 36)
    private String requestId;
    
    @Column(name = "IP", nullable = false, length = 16)
    private String ip;
    
    @Column(name = "DEVICE_ID", nullable = true, length = 20)
    private String deviceId;
    
    @Column(name = "APP_VERSION", nullable = false, length = 10)
    private String appVersion;
    
    @Column(name = "TOKEN", nullable = true, length = 50)
    private String token;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ACTIVED_TIME", nullable = true)
    private Date activedTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "EXPIRED_TIME", nullable = true)
    private Date expiredTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS", nullable = true)
    private Status status;
    
    public static enum Status {
    	ACTIVED, INACTIVED, EXPIRED
    }

    public String getRequestId() {
	return requestId;
    }

    public void setRequestId(String requestId) {
	this.requestId = requestId;
    }

    public String getIp() {
	return ip;
    }

    public void setIp(String ip) {
	this.ip = ip;
    }
    
    public String getAppVersion() {
    	return appVersion;
    }

    public void setAppVersion(String appVersion) {
    	this.appVersion = appVersion;
    }

    public Date getActivedTime() {
	return activedTime;
    }

    public void setActivedTime(Date activedTime) {
	this.activedTime = activedTime;
    }

    public Date getExpiredTime() {
	return expiredTime;
    }

    public void setExpiredTime(Date expiredTime) {
	this.expiredTime = expiredTime;
    }

    public Status getStatus() {
	return status;
    }

    public void setStatus(Status status) {
	this.status = status;
    }

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
