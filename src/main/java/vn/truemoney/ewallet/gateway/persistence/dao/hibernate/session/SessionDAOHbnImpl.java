package vn.truemoney.ewallet.gateway.persistence.dao.hibernate.session;

import java.util.Date;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import vn.truemoney.ewallet.gateway.persistence.dao.api.session.SessionDAO;
import vn.truemoney.ewallet.gateway.persistence.model.session.Session;
import vn.truemoney.framework.persistence.hinernate.dao.GeneratedIdDAOHbnImpl;

@Repository
public class SessionDAOHbnImpl extends GeneratedIdDAOHbnImpl<Session> implements SessionDAO {
	@Override
	public Class<Session> getEntityClass() {
		return Session.class;
	}

	@Override
	public Session findBySessionId(String sessionId) {
		Query query = getSession().createQuery(
				"From " + getEntityClass().getName() + " s where s.sessionId = :sessionId and s.status = :status");
		query.setParameter("sessionId", sessionId);
		query.setParameter("status", Session.Status.ACTIVED);
		return (Session) query.uniqueResult();
	}

	@Override
	public int inActiveSession(Date expiredTime) {
		Query query = getSession().createQuery("update " + getEntityClass().getName()
				+ " s set s.status = :status where s.expiredTime < :expiredTime");
		query.setTimestamp("expiredTime", expiredTime);
		query.setParameter("status", Session.Status.INACTIVED);
		return query.executeUpdate();
	}
}
