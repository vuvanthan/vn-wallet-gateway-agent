package vn.truemoney.ewallet.gateway.persistence.dao.hibernate.transaction;

import org.springframework.stereotype.Repository;
import vn.truemoney.ewallet.gateway.persistence.dao.api.transaction.TransactionLogDAO;
import vn.truemoney.ewallet.gateway.persistence.model.transaction.TransactionLog;
import vn.truemoney.framework.persistence.hinernate.dao.GeneratedIdDAOHbnImpl;

@Repository
public class TransactionLogDAOHbnImpl extends GeneratedIdDAOHbnImpl<TransactionLog> implements TransactionLogDAO {
	@Override
	public Class<TransactionLog> getEntityClass() {
		return TransactionLog.class;
	}
}
