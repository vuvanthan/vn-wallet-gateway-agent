package vn.truemoney.ewallet.gateway.persistence.dao.api.transaction;

import vn.truemoney.ewallet.gateway.persistence.model.transaction.TransactionLog;
import vn.truemoney.framework.persistence.dao.GeneratedIdDAO;

public interface TransactionLogDAO extends GeneratedIdDAO<TransactionLog> {
	
}
