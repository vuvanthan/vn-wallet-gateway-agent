package vn.truemoney.ewallet.gateway.persistence.dao.api.factory;

import vn.truemoney.ewallet.gateway.persistence.dao.api.session.SessionDAO;
import vn.truemoney.ewallet.gateway.persistence.dao.api.transaction.TransactionLogDAO;

public interface DaoFactory {
  
  SessionDAO getSessionDAO();
  
  TransactionLogDAO getTransactionLogDAO();

}
