package vn.truemoney.ewallet.gateway.common;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class ServiceFinder {
	@SuppressWarnings("rawtypes")
	private final static Class DEBUGGED_CLASS = ServiceFinder.class;
	private static Debugger debugger = new Debugger(DEBUGGED_CLASS);

	private static ApplicationContext ctx = null;

	public static ApplicationContext getContext(HttpServletRequest httpRequest) {
		return ctx = WebApplicationContextUtils
				.getRequiredWebApplicationContext(httpRequest.getSession().getServletContext());
	}

	public static ApplicationContext getContext(ServletContext context) {
		return ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
	}

	public static ApplicationContext getContext() {
		debugger.logDebug("ApplicationContext", ctx);
		return ctx;
	}

	public static void setContext(ApplicationContext applicationContext) {
		debugger.logDebug("ApplicationContext", applicationContext);
		ctx = applicationContext;
	}
}
