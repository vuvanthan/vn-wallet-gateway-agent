package vn.truemoney.ewallet.gateway.common;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class Debugger {
	protected Logger logger;
	protected final Logger debugLog = Logger.getLogger("debugLogger");

	public Debugger() {
	}

	@SuppressWarnings("rawtypes")
	public Debugger(Class clazz) {
		if (clazz != null) {
			this.logger = Logger.getLogger(clazz);
		} else {
			this.logger = debugLog;
		}
	}

	public Debugger(String moduleName) {
		if (StringUtils.isNotBlank(moduleName)) {
			this.logger = Logger.getLogger(moduleName);
		} else {
			this.logger = debugLog;
		}
	}

	public void logInfo(Object title, Object obj) {
		Logger logger = this.logger;
		logger.info(obj);
	}

	public void logError(Object title, Object obj) {
		Logger logger = this.logger;
		logger.error(obj);
	}

	public void logDebug(Object title, Object obj) {
		Logger logger = this.logger;
		logger.debug(obj);
	}

	public void logWarn(Object title, Object obj) {
		Logger logger = this.logger;
		logger.warn(obj);
	}

	public void errorException(Exception error) {
		this.logger.error(error);
	}

	public void errorException(Object message, Exception error) {
		this.logger.error(message, error);
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

}
