package vn.truemoney.ewallet.gateway.common;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.Mac;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class HmacSHA256 {
	public static final String PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA1";
	public static final int SALT_BYTES = 24;
	public static final int HASH_BYTES = 24;
	public static final int PBKDF2_ITERATIONS = 1000;

	private static String pbkdf2(String password) {
		try {
			return DatatypeConverter.printBase64Binary(
					pbkdf2(password.toCharArray(), new byte[SALT_BYTES], PBKDF2_ITERATIONS, HASH_BYTES));
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			//e.printStackTrace();
			return password;
		}
	}

	private static byte[] pbkdf2(char[] password, byte[] salt, int iterations, int bytes)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
		SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
		return skf.generateSecret(spec).getEncoded();
	}

	public static String getSignature(String password, String content, boolean defaultFlag) {
		String key = null;
		if (defaultFlag) {
			key = password;
		} else {
			key = pbkdf2(password);
		}
		try {
			Mac sha256_HMAC1 = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), "HmacSHA256");
			sha256_HMAC1.init(secret_key);

			String signature = DatatypeConverter.printBase64Binary(sha256_HMAC1.doFinal(content.getBytes()));
			return signature;
		} catch (InvalidKeyException | NoSuchAlgorithmException e) {
			//e.printStackTrace();
			return key;
		}
	}

	public static void main(String[] args) {
		String sign1 = getSignature("prnassw0Rd!", "{\"app_version\":\"1234\"}", true);
		String sign2 = getSignature("1", "123", true);

//		if (sign1.equals(sign2)) {
//			//System.out.println("true");
//		} else {
//			//System.out.println("false");
//		}
	}

}
