package vn.truemoney.ewallet.gateway.common.util;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import vn.truemoney.ewallet.gateway.SharedConstants;
import vn.truemoney.ewallet.gateway.common.Debugger;
import vn.truemoney.ewallet.gateway.common.HmacSHA256;
import vn.truemoney.ewallet.gateway.contract.common.FailResponseModel;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;
import vn.truemoney.ewallet.gateway.persistence.model.session.Session;

public class ResponseUtils {
	private static Debugger debugger = new Debugger(ResponseUtils.class);
	private static final String EXPIRED_SESSION = "G01";
	private static final String FAILED_SIGNATURE = "G02";
	private static final String BAD_REQUEST = "G03";
	private static final String BAD_GATEWAY = "G04";
	private static final String BAD_PROXY = "G05";

	@SuppressWarnings("rawtypes")
	public static void getRequestLog(HttpServletRequest request, String requestBody, Class debuggedClass) {
		Debugger requestDebugger = new Debugger(debuggedClass);
		requestDebugger.logDebug("REQUEST FROM IP", Utils.getIP(request));
		requestDebugger.logDebug("REQUEST FROM ADDRESS",
				request.getRequestURL() != null ? request.getRequestURL().toString().replaceAll(".{5}$", "*****")
						: null);
		requestDebugger.logInfo("REQUEST METHOD", request.getMethod());
		requestDebugger.logInfo("REQUEST BODY ", Utils.masking(requestBody));
	}

	// generate signature
	public static String generateSignature(String pvKey) {
		String signature;
		try {
			byte[] privateKeyBytes = DatatypeConverter.parseBase64Binary(pvKey);

			// rebuild key
			KeyFactory kf = KeyFactory.getInstance("RSA");
			PrivateKey privateKey = kf.generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));

			// generate signature
			Signature sig = Signature.getInstance("MD5WithRSA");
			sig.initSign(privateKey);
			byte[] signatureBytes = sig.sign();

			// encode data on your side using BASE64
			signature = DatatypeConverter.printBase64Binary(signatureBytes);

		} catch (NoSuchAlgorithmException e) {
			debugger.errorException(e.getMessage(), e);
			return "";
		} catch (InvalidKeySpecException e) {
			debugger.errorException(e.getMessage(), e);
			return "";
		} catch (InvalidKeyException e) {
			debugger.errorException(e.getMessage(), e);
			return "";
		} catch (SignatureException e) {
			debugger.errorException(e.getMessage(), e);
			return "";
		}
		return signature;
	}

	public static HttpHeaders getHttpHeaders() {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Signature", generateSignature(SharedConstants.PRIVATE_KEY));
		return responseHeaders;
	}

	// return fail response because of client
	public static ResponseEntity<?> returnExpiredSession() {
		FailResponseModel failResponse = new FailResponseModel(EXPIRED_SESSION,
				"Session has expired. Please sign in again!", null);
		debugger.logError("ERROR RESPONSE FOR CLIENT", failResponse);
		return new ResponseEntity<FailResponseModel>(failResponse, getHttpHeaders(), HttpStatus.PRECONDITION_FAILED);
	}

	public static ResponseEntity<?> returnUnAuthorized() {
		FailResponseModel failResponse = new FailResponseModel(FAILED_SIGNATURE, "Signature fail!", null);
		debugger.logError("ERROR RESPONSE FOR CLIENT", failResponse);
		return new ResponseEntity<FailResponseModel>(failResponse, getHttpHeaders(), HttpStatus.UNAUTHORIZED);
	}

	public static ResponseEntity<?> returnBadRequest(ResponseDTO response) {
		FailResponseModel failResponse = new FailResponseModel(response.getCode(), response.getMessage(), null);
		debugger.logError("ERROR RESPONSE FOR CLIENT", failResponse);
		return new ResponseEntity<FailResponseModel>(failResponse, getHttpHeaders(), HttpStatus.BAD_REQUEST);
	}

	// return fail response because of gateway
	public static ResponseEntity<?> returnBadGateway() {
		FailResponseModel failResponse = new FailResponseModel(BAD_GATEWAY, "Bad Gateway!", null);
		debugger.logError("ERROR RESPONSE FOR CLIENT", failResponse);
		return new ResponseEntity<FailResponseModel>(failResponse, getHttpHeaders(), HttpStatus.BAD_GATEWAY);
	}

	// return fail response because of proxy
	public static ResponseEntity<?> returnBadProxy() {
		FailResponseModel failResponse = new FailResponseModel(BAD_PROXY, "Service Unavailable!", null);
		debugger.logError("ERROR RESPONSE FOR CLIENT", failResponse);
		return new ResponseEntity<FailResponseModel>(failResponse, getHttpHeaders(), HttpStatus.SERVICE_UNAVAILABLE);
	}

	// verify
	public static void verifySignatureWithDefaultKey(String clientSignature, String body) throws Exception {
		if (!clientSignature.equals(HmacSHA256.getSignature("prnassw0Rd!", body, true))) {
			throw new Exception(FAILED_SIGNATURE);
		}
	}

	public static void verifySession(Session session) throws Exception {
		if (session == null) {
			throw new Exception(EXPIRED_SESSION);
		}
	}

	public static void verifySignature(String clientSignature, String key, String body, boolean defaultFlag)
			throws Exception {
		if (!clientSignature.equals(HmacSHA256.getSignature(key, body, defaultFlag))) {
			throw new Exception(FAILED_SIGNATURE);
		}
	}

	public static void verifyProxyResponse(ResponseDTO response) throws Exception {
		if (response == null) {
			throw new Exception(BAD_PROXY);
		} else {
			if (!response.getCode().equals("00")) {
				throw new Exception(BAD_REQUEST);
			}
		}

	}

	// return error response for client
	public static ResponseEntity<?> returnError(String errorCode, ResponseDTO response) {
		switch (errorCode) {
		case EXPIRED_SESSION:
			return returnExpiredSession();

		case FAILED_SIGNATURE:
			return returnUnAuthorized();

		case BAD_REQUEST:
			return returnBadRequest(response);

		case BAD_PROXY:
			return returnBadProxy();

		default:
			return returnBadGateway();
		}
	}
}
