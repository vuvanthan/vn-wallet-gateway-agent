package vn.truemoney.ewallet.gateway.integration.service.auth.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import vn.truemoney.ewallet.gateway.contract.auth.RefreshTokenRequest;
import vn.truemoney.ewallet.gateway.contract.auth.SignInRequest;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;
import vn.truemoney.ewallet.gateway.integration.service.AbstractAuthenticationService;
import vn.truemoney.ewallet.gateway.integration.service.auth.ISignInOutService;

@Service
public class SignInOutService extends AbstractAuthenticationService implements ISignInOutService {
	@Value("${application.integration.signin-uri}")
	private String SIGNIN_URI;
	
	@Value("${application.integration.signout-uri}")
	private String SIGNOUT_URI;
	
	@Value("${application.integration.refreshtoken-uri}")
	private String REFRESH_TOKEN_URI;
	
	@Override
	public ResponseDTO signIn(SignInRequest request) {
		ResponseDTO response = callPost(SIGNIN_URI, null ,request);
		return response;
	}

	@Override
	public ResponseDTO signOut(String token) {
		ResponseDTO response = callPost(SIGNOUT_URI, token ,null);
		return response;
	}

	@Override
	public ResponseDTO refereshToken(String token, RefreshTokenRequest request) {
		ResponseDTO response = callPost(REFRESH_TOKEN_URI, token, request);				
		return response;
	}
}
