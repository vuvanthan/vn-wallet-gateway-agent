package vn.truemoney.ewallet.gateway.integration.service.auth;

import vn.truemoney.ewallet.gateway.contract.auth.RefreshTokenRequest;
import vn.truemoney.ewallet.gateway.contract.auth.SignInRequest;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;

public interface ISignInOutService {
	
	ResponseDTO signIn(SignInRequest request);
	
	ResponseDTO signOut(String token);
	
	ResponseDTO refereshToken(String token,RefreshTokenRequest request);
}
