package vn.truemoney.ewallet.gateway.integration.service.billpay.impl;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import vn.truemoney.ewallet.gateway.contract.billpay.BillPayRequest;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;
import vn.truemoney.ewallet.gateway.integration.service.AbstractProxyService;
import vn.truemoney.ewallet.gateway.integration.service.billpay.IBillPayService;

@Service
public class BillPayService extends AbstractProxyService implements IBillPayService{
	
	@Value("${application.integration.service_billpay-uri}")
	private String SERVICE_URI;
	
	@Value("${application.integration.service_billpay-info-uri}")
	private String BITLLPAY_INFO_URI;
	
	@Value("${application.integration.service_billpay-order-uri}")
	private String BITLLPAY_ORDER_URI;
	
	@Value("${application.integration.service_billpay-confirm-uri}")
	private String BITLLPAY_CONFORM_URI;
	
	@Value("${application.integration.service_billpay-resendotp-uri}")
	private String BITLLPAY_RESENDOTP_URI;
	
	private final String OTP_TYPE = "otp_type";
	private final int OTP_TYPE_VALUE = 20;
	
	@Override
	public ResponseDTO getListServiceAndProvider(String token) {
		ResponseDTO response = callGet(SERVICE_URI, token);
		return response;
	}

	@Override
	public ResponseDTO requestBillPayInfo(String token, BillPayRequest requestBody) {
		ResponseDTO response = callPost(BITLLPAY_INFO_URI, token, requestBody);
		return response;
	}

	@Override
	public ResponseDTO createOrderBillPay(String token, BillPayRequest requestBody) {
		ResponseDTO response = callPost(BITLLPAY_ORDER_URI, token, requestBody);
		return response;
	}

	@Override
	public ResponseDTO confirmPurchase(String token, BillPayRequest requestBody) {
		String url = BITLLPAY_CONFORM_URI.replace("{order_id}",requestBody.getOrder_id());
		ResponseDTO response = callPost(url, token, requestBody);
		return response;
	}

	@Override
	public ResponseDTO resendOTP(String token, BillPayRequest requestBody) {
		String card_uri = BITLLPAY_RESENDOTP_URI.replace("{verify_token}", requestBody.getVerify_token());
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put(OTP_TYPE, OTP_TYPE_VALUE);		
		ResponseDTO response = callPost(card_uri, token, data);
		return response;
	}
	
}
