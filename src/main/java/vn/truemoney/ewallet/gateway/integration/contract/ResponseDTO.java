package vn.truemoney.ewallet.gateway.integration.contract;

import java.io.Serializable;
import java.util.HashMap;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseDTO implements Serializable {
	private String code;
	@JsonProperty("msg")
	private String message;
	private HashMap<String, Object> data;

	public String getCode() {
		if(code.equals("0")){
			return "00";
		}
		else{
			return code;
		}
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public HashMap<String, Object> getData() {
		return data;
	}

	public void setData(HashMap<String, Object> data) {
		this.data = data;
	}
}