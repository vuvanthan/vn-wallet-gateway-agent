package vn.truemoney.ewallet.gateway.integration.service.billpay;

import vn.truemoney.ewallet.gateway.contract.billpay.BillPayRequest;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;

public interface IBillPayService {
	ResponseDTO getListServiceAndProvider(String token);
	ResponseDTO requestBillPayInfo(String token, BillPayRequest requestBody);
	ResponseDTO createOrderBillPay(String token, BillPayRequest requestBody);
	ResponseDTO confirmPurchase(String token, BillPayRequest requestBody);
	ResponseDTO resendOTP(String token, BillPayRequest requestBody);
}
