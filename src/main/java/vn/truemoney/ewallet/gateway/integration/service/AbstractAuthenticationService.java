package vn.truemoney.ewallet.gateway.integration.service;

import org.springframework.beans.factory.annotation.Autowired;

import vn.truemoney.ewallet.gateway.common.Debugger;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;
import vn.truemoney.ewallet.gateway.integration.proxyAndOther.EWalletAuthentication;

public class AbstractAuthenticationService {
	private Debugger debugger = new Debugger(AbstractAuthenticationService.class);

	@Autowired
	protected EWalletAuthentication ewalletAuthentication;

	protected ResponseDTO callGet(String URI, String token) throws SecurityException {
		try {
			ResponseDTO response = (ResponseDTO) ewalletAuthentication.callGetRequest(URI, token);

			return response;
		} catch (Exception e) {
			debugger.logError(e.getMessage(), e);
			return null;
		}
	}

	protected ResponseDTO callPost(String URI, String token, Object request) throws SecurityException {
		try {
			ResponseDTO response = (ResponseDTO) ewalletAuthentication.callPostRequest(URI, token, request);
			return response;
		} catch (Exception e) {
			debugger.logError(e.getMessage(), e);
			return null;
		}
	}
	
	protected ResponseDTO callPut(String URI, String token, Object request) throws SecurityException {
		try {
			ResponseDTO response = (ResponseDTO) ewalletAuthentication.callPutRequest(URI, token, request);
			return response;
		} catch (Exception e) {
			debugger.logError(e.getMessage(), e);
			return null;
		}
	}
	
	protected ResponseDTO callDelete(String URI, String token, Object request) throws SecurityException {
		try {
			ResponseDTO response = (ResponseDTO) ewalletAuthentication.callDeleteRequest(URI, token, request);
			return response;
		} catch (Exception e) {
			debugger.logError(e.getMessage(), e);
			return null;
		}
	}
	
	protected ResponseDTO callPostSignUp(String URI, String token, Object request, String deviceId, String deviceDescription) throws SecurityException {
		try {
			ResponseDTO response = (ResponseDTO) ewalletAuthentication.callPostSigUpRequest(URI, token, request, deviceId, deviceDescription);
			return response;
		} catch (Exception e) {
			debugger.logError(e.getMessage(), e);
			return null;
		}
	}
}
