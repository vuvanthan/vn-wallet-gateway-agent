package vn.truemoney.ewallet.gateway.integration.service.auth;

import vn.truemoney.ewallet.gateway.contract.auth.SignUpRequest;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;

public interface ISignUpService {
	ResponseDTO getCustomer(SignUpRequest request);   
	
	ResponseDTO resendOTP(SignUpRequest request);
	
	ResponseDTO verifyOTP(SignUpRequest request);
	
	ResponseDTO signUp(SignUpRequest request, String deviceId, String deviceDescription);
}
