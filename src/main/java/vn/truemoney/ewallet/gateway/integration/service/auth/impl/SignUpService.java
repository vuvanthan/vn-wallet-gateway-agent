package vn.truemoney.ewallet.gateway.integration.service.auth.impl;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import vn.truemoney.ewallet.gateway.contract.auth.SignUpRequest;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;
import vn.truemoney.ewallet.gateway.integration.service.AbstractAuthenticationService;
import vn.truemoney.ewallet.gateway.integration.service.auth.ISignUpService;

@Service
public class SignUpService extends AbstractAuthenticationService implements ISignUpService {

	@Value("${application.integration.checkinfo-uri}")
	private String CHECKINFO_URI;
	
	@Value("${application.integration.resendotp-uri}")
	private String RESENDOTP_URI;
	
	@Value("${application.integration.verifyotp-uri}")
	private String VERIFYOTP_URI;
	
	@Value("${application.integration.signup-uri}")
	private String SIGNUP_URI;
	
	private final String OTP_TYPE = "otp_type";
	private final int OTP_TYPE_VALUE = 10;

	@Override
	public ResponseDTO getCustomer(SignUpRequest request) {
		ResponseDTO response = callPost(CHECKINFO_URI, null, request);
		return response;
	}
	
	@Override
	public ResponseDTO resendOTP(SignUpRequest request) {
		String uri = RESENDOTP_URI.replace("{verify_token}",request.getVerify_token());
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put(OTP_TYPE, OTP_TYPE_VALUE);
		ResponseDTO response = callPost(uri, null, data);
		return response;
	}
	
	@Override
	public ResponseDTO verifyOTP(SignUpRequest request) {
		String uri = VERIFYOTP_URI.replace("{verify_token}",request.getVerify_token());
		ResponseDTO response = callPost(uri, null, request);
		return response;
	}

	@Override
	public ResponseDTO signUp(SignUpRequest request, String deviceId, String deviceDescription) {
		ResponseDTO response = callPostSignUp(SIGNUP_URI, null, request,deviceId, deviceDescription);
		return response;
	}

}
