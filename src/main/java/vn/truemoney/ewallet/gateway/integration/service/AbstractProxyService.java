package vn.truemoney.ewallet.gateway.integration.service;

import org.springframework.beans.factory.annotation.Autowired;

import vn.truemoney.ewallet.gateway.common.Debugger;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;
import vn.truemoney.ewallet.gateway.integration.proxyAndOther.EWalletProxy;

public abstract class AbstractProxyService {
	private Debugger debugger = new Debugger(AbstractProxyService.class);

	@Autowired
	protected EWalletProxy ewalletProxy;

	protected ResponseDTO callGet(String URI, String token) throws SecurityException {
		try {
			ResponseDTO response = (ResponseDTO) ewalletProxy.callGetRequest(URI, token);

			return response;
		} catch (Exception e) {
			debugger.logError(e.getMessage(), e);
			return null;
		}
	}

	protected ResponseDTO callPost(String URI, String token, Object request) throws SecurityException {
		try {
			ResponseDTO response = (ResponseDTO) ewalletProxy.callPostRequest(URI, token, request);
			return response;
		} catch (Exception e) {
			debugger.logError(e.getMessage(), e);
			return null;
		}
	}
	
	protected ResponseDTO callPut(String URI, String token, Object request) throws SecurityException {
		try {
			ResponseDTO response = (ResponseDTO) ewalletProxy.callPutRequest(URI, token, request);
			return response;
		} catch (Exception e) {
			debugger.logError(e.getMessage(), e);
			return null;
		}
	}
	
	protected ResponseDTO callDelete(String URI, String token, Object request) throws SecurityException {
		try {
			ResponseDTO response = (ResponseDTO) ewalletProxy.callDeleteRequest(URI, token, request);
			return response;
		} catch (Exception e) {
			debugger.logError(e.getMessage(), e);
			return null;
		}
	}
}
