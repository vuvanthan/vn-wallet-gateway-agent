package vn.truemoney.ewallet.gateway.integration.proxyAndOther;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

import vn.truemoney.ewallet.gateway.common.Debugger;
import vn.truemoney.ewallet.gateway.common.util.HttpUtil;
import vn.truemoney.ewallet.gateway.common.util.Utils;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;

@Service
public class EWalletAuthentication {

	private Debugger debugger = new Debugger(EWalletAuthentication.class);
	private String BEARER = "Bearer ";
	@Value("${application.integration.authentication-base-url}")
	protected String AUTHENTICATION_BASE_URL;

	public Object callGetRequest(String requestURI, String token) throws SecurityException {
		try {
			String url = AUTHENTICATION_BASE_URL + requestURI;
			debugger.logInfo("URL AUTHENTICATION", url.replaceAll(".{3}$", "***"));
			HttpHeaders headers = new HttpHeaders();
			if (token != null) {
				headers.add("Authorization", BEARER + token);
			}
			headers.add("Content-Type", "application/json");

			HttpEntity<?> httpEntity = new HttpEntity<Object>(null, headers);

			HttpUtil.restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
			ResponseEntity<ResponseDTO> resp = HttpUtil.restTemplate.exchange(url, HttpMethod.GET, httpEntity,
					ResponseDTO.class);
			return resp.getBody();
		} catch (HttpStatusCodeException e) {
			return Utils.checkHttpStatus(e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object callPostRequest(String requestURI, String token, Object request) throws SecurityException {
		try {
			String url = AUTHENTICATION_BASE_URL + requestURI;
			debugger.logInfo("URL AUTHENTICATION", url.replaceAll(".{3}$", "***"));
			HttpHeaders headers = new HttpHeaders();
			if (token != null) {
				headers.add("Authorization", BEARER + token);
			}
			headers.add("Content-Type", "application/json");

			final HttpEntity httpEntity = new HttpEntity(request, headers);

			HttpUtil.restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
			ResponseEntity<ResponseDTO> resp = HttpUtil.restTemplate.exchange(url, HttpMethod.POST, httpEntity,
					ResponseDTO.class);
			return resp.getBody();
		} catch (HttpStatusCodeException e) {
			return Utils.checkHttpStatus(e);

		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object callPutRequest(String requestURI, String token, Object request) throws SecurityException {
		try {
			String url = AUTHENTICATION_BASE_URL + requestURI;
			debugger.logInfo("URL AUTHENTICATION", url.replaceAll(".{3}$", "***"));
			HttpHeaders headers = new HttpHeaders();
			if (token != null) {
				headers.add("Authorization", BEARER + token);
			}
			headers.add("Content-Type", "application/json");

			final HttpEntity httpEntity = new HttpEntity(request, headers);

			HttpUtil.restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
			ResponseEntity<ResponseDTO> resp = HttpUtil.restTemplate.exchange(url, HttpMethod.PUT, httpEntity,
					ResponseDTO.class);
			return resp.getBody();
		} catch (HttpStatusCodeException e) {
			return Utils.checkHttpStatus(e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object callDeleteRequest(String requestURI, String token, Object request) throws SecurityException {
		try {
			String url = AUTHENTICATION_BASE_URL + requestURI;
			debugger.logInfo("URL AUTHENTICATION", url.replaceAll(".{3}$", "***"));
			HttpHeaders headers = new HttpHeaders();
			if (token != null) {
				headers.add("Authorization", BEARER + token);
			}
			headers.add("Content-Type", "application/json");

			final HttpEntity httpEntity = new HttpEntity(request, headers);

			HttpUtil.restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
			ResponseEntity<ResponseDTO> resp = HttpUtil.restTemplate.exchange(url, HttpMethod.DELETE, httpEntity,
					ResponseDTO.class);
			return resp.getBody();
		} catch (HttpStatusCodeException e) {
			return Utils.checkHttpStatus(e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object callPostSigUpRequest(String requestURI, String token, Object request, String deviceId,
			String deviceDescription) throws SecurityException {
		try {
			String url = AUTHENTICATION_BASE_URL + requestURI;
			debugger.logInfo("URL AUTHENTICATION", url.replaceAll(".{3}$", "***"));
			HttpHeaders headers = new HttpHeaders();
			if (token != null) {
				headers.add("Authorization", BEARER + token);
			}

			if (deviceId != null) {
				headers.add("device_id", deviceId);
			}
			
			if (deviceDescription != null) {
				headers.add("device_description", deviceDescription);
			}
			
			headers.add("Content-Type", "application/json");

			final HttpEntity httpEntity = new HttpEntity(request, headers);

			HttpUtil.restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
			ResponseEntity<ResponseDTO> resp = HttpUtil.restTemplate.exchange(url, HttpMethod.POST, httpEntity,
					ResponseDTO.class);
			return resp.getBody();
		} catch (HttpStatusCodeException e) {
			return Utils.checkHttpStatus(e);

		}
	}
}
