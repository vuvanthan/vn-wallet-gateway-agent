package vn.truemoney.ewallet.gateway.businesslogic.util;

import vn.truemoney.framework.service.api.MobiliserServiceException;

public class SystemConfigurationErrorException extends MobiliserServiceException {
  private static final long serialVersionUID = 1L;

  public SystemConfigurationErrorException() {}

  public SystemConfigurationErrorException(String message) {
    super(message);
  }

  public SystemConfigurationErrorException(Throwable cause) {
    super(cause);
  }

  public SystemConfigurationErrorException(String message, Throwable cause) {
    super(message, cause);
  }

  public int getErrorCode() {
    return 199;
  }
}
