package vn.truemoney.ewallet.gateway.businesslogic.session;

import vn.truemoney.ewallet.gateway.persistence.model.session.Session;

public interface ISessionLogic {

	public void createSession(Session session, Long callerId);

	public Session findBySessionId(String sessionId);

	public void inActiveSession();
}
