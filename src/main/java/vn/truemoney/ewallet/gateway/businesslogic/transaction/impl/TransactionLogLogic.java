package vn.truemoney.ewallet.gateway.businesslogic.transaction.impl;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.truemoney.ewallet.gateway.businesslogic.transaction.ITransactionLogLogic;
import vn.truemoney.ewallet.gateway.common.Debugger;
import vn.truemoney.ewallet.gateway.persistence.dao.api.factory.DaoFactory;
import vn.truemoney.ewallet.gateway.persistence.model.transaction.TransactionLog;


@Service
@Transactional
public class TransactionLogLogic implements ITransactionLogLogic, InitializingBean {
	private static Debugger debugger = new Debugger(TransactionLogLogic.class);
	@Autowired
	private DaoFactory daoFactory;

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		if (this.daoFactory == null)
			throw new IllegalStateException("daoFactory is required");
	}

	@Override
	public void saveTransactionLog(TransactionLog transactionLog, Long callerId) {
		try {
			daoFactory.getTransactionLogDAO().save(transactionLog, callerId);
		} catch (Exception e) {
			debugger.errorException(e);
		}
	}
}
