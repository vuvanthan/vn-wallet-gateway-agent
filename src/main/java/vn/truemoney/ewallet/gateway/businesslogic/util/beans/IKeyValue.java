package vn.truemoney.ewallet.gateway.businesslogic.util.beans;

import java.io.Serializable;

public interface IKeyValue extends Serializable {

  public String getKey();

  public String getValue();
}
