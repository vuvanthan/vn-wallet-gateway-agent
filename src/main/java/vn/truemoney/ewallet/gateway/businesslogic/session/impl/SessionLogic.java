package vn.truemoney.ewallet.gateway.businesslogic.session.impl;

import java.util.Date;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import vn.truemoney.ewallet.gateway.businesslogic.session.ISessionLogic;
import vn.truemoney.ewallet.gateway.common.Debugger;
import vn.truemoney.ewallet.gateway.persistence.dao.api.factory.DaoFactory;
import vn.truemoney.ewallet.gateway.persistence.model.session.Session;

@Service
@Transactional
@EnableScheduling
public class SessionLogic implements ISessionLogic, InitializingBean {
	private static Debugger debugger = new Debugger(SessionLogic.class);
	@Autowired
	private DaoFactory daoFactory;

	@Autowired
	private TransactionTemplate transactionTemplate;
	
	@Value("${system.instance.id}")
	private String instanceId;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		if (this.daoFactory == null)
			throw new IllegalStateException("daoFactory is required");
	}

	@Override
	public void createSession(Session session, Long callerId) {
		try {
			daoFactory.getSessionDAO().save(session, callerId);
		} catch (Exception e) {
			debugger.errorException(e);
		}
	}

	@Override
	public Session findBySessionId(String sessionId) {
		try {
			return daoFactory.getSessionDAO().findBySessionId(sessionId);
		} catch (Exception e) {
			debugger.errorException(e);
			return null;
		}
	}

	@Override
	@Scheduled(cron = "${application.cron.session.expression}")
	public void inActiveSession() {
		if(!instanceId.equals("001")) {
			return;
		}
		try {
			transactionTemplate.execute(new TransactionCallbackWithoutResult() {
				@Override
				protected void doInTransactionWithoutResult(TransactionStatus status) {
					final Date expiredTime = new Date();
					daoFactory.getSessionDAO().inActiveSession(expiredTime);
				}
			});
		} catch (Exception e) {
			debugger.errorException(e);
		}

	}

}
