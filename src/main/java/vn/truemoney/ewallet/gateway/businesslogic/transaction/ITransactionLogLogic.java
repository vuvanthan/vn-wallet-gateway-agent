package vn.truemoney.ewallet.gateway.businesslogic.transaction;

import vn.truemoney.ewallet.gateway.persistence.model.transaction.TransactionLog;

public interface ITransactionLogLogic {

	public void saveTransactionLog(TransactionLog transactionLog, Long callerId);

}
