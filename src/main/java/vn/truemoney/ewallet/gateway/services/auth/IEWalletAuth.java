package vn.truemoney.ewallet.gateway.services.auth;

import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;

public interface IEWalletAuth {
	// sign up
	ResponseDTO checkInfo(String requestBody, String deviceId);

	ResponseDTO resendOTP(String requestBody);

	ResponseDTO verifyOTP(String requestBody);

	ResponseDTO signUp(String requestBody, String deviceId, String publicIp, String devicedDescript);

	// sign in
	ResponseDTO signIn(String requestBody, String deviceId, String publicIp);

	// sign out
	ResponseDTO signOut(String token);

	// refresh Token
	ResponseDTO refreshToken(String token, String deviceId, String publicIp,String request);
}
