package vn.truemoney.ewallet.gateway.services.billpay.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import vn.truemoney.ewallet.gateway.contract.billpay.BillPayRequest;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;
import vn.truemoney.ewallet.gateway.integration.service.billpay.IBillPayService;
import vn.truemoney.ewallet.gateway.services.billpay.IEWalletBillPay;

@Component
public class EWalletBillPay implements IEWalletBillPay {

	Gson gson = new Gson();
	@Autowired
	private IBillPayService iBillPayService;

	@Override
	public ResponseDTO getListServiceAndProvider(String token) {
		ResponseDTO response = iBillPayService.getListServiceAndProvider(token);
		return response;
	}

	@Override
	public ResponseDTO requestBillPayInfo(String token, String requestBody) {
		BillPayRequest request = gson.fromJson(requestBody, BillPayRequest.class);
		ResponseDTO response = iBillPayService.requestBillPayInfo(token, request);
		return response;
	}

	@Override
	public ResponseDTO createOrderBillPay(String token, String requestBody) {
		BillPayRequest request = gson.fromJson(requestBody, BillPayRequest.class);
		ResponseDTO response = iBillPayService.createOrderBillPay(token, request);
		return response;
	}

	@Override
	public ResponseDTO conformPurchase(String token, String requestBody) {
		BillPayRequest request = gson.fromJson(requestBody, BillPayRequest.class);
		ResponseDTO response = iBillPayService.confirmPurchase(token, request);
		return response;
	}

	@Override
	public ResponseDTO resendOTP(String token, String requestBody) {
		BillPayRequest request = gson.fromJson(requestBody, BillPayRequest.class);
		ResponseDTO response = iBillPayService.resendOTP(token, request);
		return response;
	}
	
}
