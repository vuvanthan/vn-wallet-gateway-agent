package vn.truemoney.ewallet.gateway.services.billpay;

import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;

public interface IEWalletBillPay {

	ResponseDTO getListServiceAndProvider(String token);
	ResponseDTO requestBillPayInfo(String token, String requestBody);
	ResponseDTO createOrderBillPay(String token,String requestBody);
	ResponseDTO conformPurchase(String token, String requestBody);
	ResponseDTO resendOTP(String token, String requestBody);
}
