package vn.truemoney.ewallet.gateway.services.auth.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import vn.truemoney.ewallet.gateway.contract.auth.RefreshTokenRequest;
import vn.truemoney.ewallet.gateway.contract.auth.SignInRequest;
import vn.truemoney.ewallet.gateway.contract.auth.SignUpRequest;
import vn.truemoney.ewallet.gateway.integration.contract.ResponseDTO;
import vn.truemoney.ewallet.gateway.integration.service.auth.ISignInOutService;
import vn.truemoney.ewallet.gateway.integration.service.auth.ISignUpService;
import vn.truemoney.ewallet.gateway.services.auth.IEWalletAuth;

@Component
public class EWalletAuth implements IEWalletAuth{
	private Gson gson = new Gson();
	
	@Autowired
	private ISignInOutService iSignInOutService;
	
	@Autowired
	private ISignUpService iSignUpService;
	
	@Override
	public ResponseDTO checkInfo(String requestBody,String deviceId) {
		SignUpRequest request = gson.fromJson(requestBody, SignUpRequest.class);
		request.setDevice_id(deviceId);
		ResponseDTO response = iSignUpService.getCustomer(request);
		return response;
	}
	
	@Override
	public ResponseDTO resendOTP(String requestBody) {
		SignUpRequest request = gson.fromJson(requestBody, SignUpRequest.class);
		ResponseDTO response =iSignUpService.resendOTP(request);
		return response;
	}

	@Override
	public ResponseDTO verifyOTP(String requestBody) {
		SignUpRequest request = gson.fromJson(requestBody, SignUpRequest.class);
		ResponseDTO response = iSignUpService.verifyOTP(request);
		return response;
	}
	
	@Override
	public ResponseDTO signUp(String requestBody, String deviceId, String publicIp, String devicedDescript) {
		SignUpRequest request = gson.fromJson(requestBody, SignUpRequest.class);
//		request.setDevice_id(deviceId);
		request.setPublic_ip(publicIp);
		ResponseDTO response = iSignUpService.signUp(request,deviceId,devicedDescript);
		return response;
	}

	//sign in
	@Override
	public ResponseDTO signIn(String requestBody, String deviceId, String publicIp) {
		SignInRequest request = gson.fromJson(requestBody, SignInRequest.class);
		request.setDevice_id(deviceId);
		request.setPublic_ip(publicIp);
		ResponseDTO response = iSignInOutService.signIn(request);
		return response;
	}

	@Override
	public ResponseDTO signOut(String token) {
		ResponseDTO response = iSignInOutService.signOut(token);
		return response;
	}

	@Override
	public ResponseDTO refreshToken(String token, String deviceId, String publicIp, String request) {
		
		RefreshTokenRequest tokenRequest = gson.fromJson(request, RefreshTokenRequest.class);
		tokenRequest.setDevice_id(deviceId);
		tokenRequest.setPublic_ip(publicIp);
		ResponseDTO response = iSignInOutService.refereshToken(token,tokenRequest);
		return response;
	}

}
