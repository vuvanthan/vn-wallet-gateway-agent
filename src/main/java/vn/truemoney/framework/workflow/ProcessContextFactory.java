package vn.truemoney.framework.workflow;

import vn.truemoney.framework.workflow.exception.WorkflowException;

public interface ProcessContextFactory<U, T> {

  public ProcessContext<U> createContext(T preSeedData) throws WorkflowException;

}
