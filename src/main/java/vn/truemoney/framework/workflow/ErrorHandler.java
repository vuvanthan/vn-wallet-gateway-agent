package vn.truemoney.framework.workflow;

import org.springframework.beans.factory.BeanNameAware;

import vn.truemoney.framework.workflow.exception.WorkflowException;

public interface ErrorHandler extends BeanNameAware {

  public void handleError(ProcessContext<?> context, Throwable th) throws WorkflowException;

}
