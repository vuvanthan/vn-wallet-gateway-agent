package vn.truemoney.framework.persistence.dao;

import vn.truemoney.framework.persistence.model.GeneratedIdEntry;

public interface GeneratedIdDAO<T extends GeneratedIdEntry> extends UpdatableDAO<T, Long> {
  public T newInstance();
}
