package vn.truemoney.framework.persistence.dao;

public interface IInternalDAOCallerData {
  public long getCallerId();
}
