package vn.truemoney.framework.security.api;

public interface ICallerUtils {

  public long getCallerId();

  public CallerInformation getCallerInformation();

  public boolean hasPrivilege(String privilege);
  
}
