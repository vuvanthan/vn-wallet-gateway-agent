package vn.truemoney.framework.security.api;

public interface IInternalServiceCallAuthenticationData {
  public String getUsername();
}
