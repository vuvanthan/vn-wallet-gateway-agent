package com.truemoney.ewallet.test;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.Grant;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import vn.truemoney.ewallet.gateway.StartApplication;
import vn.truemoney.ewallet.gateway.common.util.Utils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = StartApplication.class)
@WebAppConfiguration
public class S3UploadTest {

	private static final String BUCKET_NAME = "vn-wallet.profile-pic.dev.us-w2";
	private static final String UPLOAD_FILE = "/home/vietpt/Pictures/index.jpeg";
	private static final String ACCESS_KEY_ID = "AKIAJA5UYCV6OFL3QAYA";
	private static final String SECRET_ACCESS_KEY = "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";
	// private static final String END_POINT_URL = "aws.amazon.com";

	@SuppressWarnings("deprecation")
	@Test
	public void uploadRequest() throws Exception {
		/*
		 * ClientConfiguration clientConfig = new ClientConfiguration();
		 * clientConfig.setProtocol(Protocol.HTTPS);
		 */

		AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY_ID, SECRET_ACCESS_KEY);
		AmazonS3 s3client = new AmazonS3Client(credentials);
		// s3client.setEndpoint(END_POINT_URL);

		try {
			AccessControlList acl = s3client.getBucketAcl(BUCKET_NAME);
			List<Grant> grants = acl.getGrantsAsList();
			for (Grant grant : grants) {
				//System.out.format("  %s: %s\n", grant.getGrantee().getIdentifier(), grant.getPermission().toString());
			}

			//System.out.println("Uploading a new object to S3 from a file\n");
			File file = new File(UPLOAD_FILE);
			FileInputStream stream = new FileInputStream(file);

			ObjectMetadata objectMetadata = new ObjectMetadata();
			objectMetadata.setContentLength(file.length());
			//System.out.println(file.length());

			String fileName = Utils.generateRandomString(8) + ".jpeg";

			PutObjectRequest putObjectRequest = new PutObjectRequest(BUCKET_NAME, fileName, stream, objectMetadata);
			s3client.putObject(putObjectRequest);

			File localFile = new File("/home/vietpt/Desktop/" + fileName);
			s3client.getObject(new GetObjectRequest(BUCKET_NAME, fileName), localFile);

			//System.out.println("Listing objects");
			ObjectListing objectListing = s3client.listObjects(new ListObjectsRequest().withBucketName(BUCKET_NAME));
			for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
				//System.out.println(" - " + objectSummary.getKey() + "  " + "(size = " + objectSummary.getSize() + ")");
			}

		} catch (AmazonServiceException ase) {
			//System.out.println("Error Message:    " + ase.getMessage() + "\n");
			//System.out.println("HTTP Status Code: " + ase.getStatusCode() + "\n");
			//System.out.println("AWS Error Code:   " + ase.getErrorCode() + "\n");
			//System.out.println("Error Type:       " + ase.getErrorType() + "\n");
			//System.out.println("Request ID:       " + ase.getRequestId() + "\n");
		} catch (AmazonClientException ace) {
			System.out.println("\nCaught an AmazonClientException, which " + "means the client encountered "
					+ "an internal error while trying to " + "communicate with S3, "
					+ "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage() + "\n");
		}
	}
}
