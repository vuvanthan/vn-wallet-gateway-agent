package com.truemoney.ewallet.test;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import vn.truemoney.ewallet.gateway.StartApplication;
import vn.truemoney.ewallet.gateway.common.HmacSHA256;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = StartApplication.class)
@WebAppConfiguration
public class ClientSignInTest {
	private String APP_VERSION = "1.2.2.1";
	private String PHONE_NUMBER = "012345678881";
	private String PIN = "999999";
	private String Token;
	private String sessionId;
	private String sessionKey;

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;

	@Before
	public void setup() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	public String getSessionRequest() throws Exception {
		String url = "/api/mobile/v1/get_session";
		JSONObject request = new JSONObject();

		request.put("app_version", APP_VERSION);
		request.put("device_id", 123);

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(url);
		requestBuilder.contentType(MediaType.APPLICATION_JSON_VALUE);
		requestBuilder.content(request.toString());
		requestBuilder.header("Signature", HmacSHA256.getSignature("prnassw0Rd!", request.toString(), true));
		requestBuilder.header("Request-Id", "817741");

		MvcResult result = mvc.perform(requestBuilder).andReturn();

		String reponse_content = result.getResponse().getContentAsString();
		//System.out.println("response: " + reponse_content);
		return reponse_content;
	}

	public void wsSignInRequest() throws Exception {
		String session = getSessionRequest();
		JSONObject sessionObject = new JSONObject(session);

		String url = "/api/mobile/v1/auth/sign_in";
		JSONObject request = new JSONObject();
		
		sessionId = sessionObject.getString("session_id");
		sessionKey = sessionObject.getString("session_key");

		request.put("phone_number", PHONE_NUMBER);
		request.put("pin", PIN);

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(url);
		requestBuilder.contentType(MediaType.APPLICATION_JSON_VALUE);
		requestBuilder.content(request.toString());
		requestBuilder.header("Signature",
				HmacSHA256.getSignature(sessionKey, request.toString(), false));
		requestBuilder.header("Request-Id", "817741");
		requestBuilder.header("Session-Id", sessionId);

		MvcResult result = mvc.perform(requestBuilder).andReturn();

		String reponse_content = result.getResponse().getContentAsString();
		JSONObject reponse_content_json = new JSONObject(reponse_content);
		//System.out.println("response: " + reponse_content);

		Token = reponse_content_json.getString("token");
	}
	
	@Test
	public void wsLinkBankRequest() throws Exception {
		wsSignInRequest();

		
		String url = "/api/mobile/v1/home/banks";
		JSONObject request = new JSONObject();

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(url);
		requestBuilder.contentType(MediaType.APPLICATION_JSON_VALUE);
		requestBuilder.header("Signature",
				HmacSHA256.getSignature(sessionKey, "", false));
		requestBuilder.header("Request-Id", "817741");
		requestBuilder.header("Session-Id", sessionId);
		requestBuilder.header("Token",Token);


		MvcResult result = mvc.perform(requestBuilder).andReturn();

		String reponse_content = result.getResponse().getContentAsString();
		//System.out.println("response: " + reponse_content);
	}
}
