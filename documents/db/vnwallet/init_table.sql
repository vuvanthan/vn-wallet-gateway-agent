/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : PostgreSQL
 Source Server Version : 90510
 Source Host           : localhost:5432
 Source Catalog        : vn_wallet_gateway
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90510
 File Encoding         : 65001

 Date: 21/11/2017 16:14:26
*/


-- ----------------------------
-- Sequence structure for vnwalletgw_session_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."vnwalletgw_session_id_seq";
CREATE SEQUENCE "public"."vnwalletgw_session_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."vnwalletgw_session_id_seq"', 479, true);

-- ----------------------------
-- Sequence structure for vnwalletgw_transaction_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."vnwalletgw_transaction_log_id_seq";
CREATE SEQUENCE "public"."vnwalletgw_transaction_log_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
 START 1
CACHE 1;
SELECT setval('"public"."vnwalletgw_transaction_log_id_seq"', 2510, true);
-- ----------------------------
-- Table structure for vnwalletgw_session
-- ----------------------------
DROP TABLE IF EXISTS "public"."vnwalletgw_session";
CREATE TABLE "public"."vnwalletgw_session" (
  "id" int8 NOT NULL DEFAULT nextval('vnwalletgw_session_id_seq'::regclass),
  "created_time" timestamp(6) DEFAULT NULL,
  "creator_id" int8 DEFAULT NULL,
  "last_updated_time" timestamp(6) DEFAULT NULL,
  "last_updated_id" int8 DEFAULT NULL,
  "actived_time" timestamp(6) DEFAULT NULL,
  "app_version" varchar(10) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "device_id" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "expired_time" timestamp(6) DEFAULT NULL,
  "ip" varchar(16) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "request_id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "session_id" varchar(16) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "session_key" varchar(32) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "status" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "token" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for vnwalletgw_transaction_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."vnwalletgw_transaction_log";
CREATE TABLE "public"."vnwalletgw_transaction_log" (
  "id" int8 NOT NULL DEFAULT nextval('vnwalletgw_transaction_log_id_seq'::regclass),
  "created_time" timestamp(6) DEFAULT NULL,
  "creator_id" int8 DEFAULT NULL,
  "last_updated_time" timestamp(6) DEFAULT NULL,
  "last_updated_id" int8 DEFAULT NULL,
  "ip" varchar(16) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "request_id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "request_time" timestamp(6) DEFAULT NULL,
  "request_uri" varchar(100) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "response_time" timestamp(6) DEFAULT NULL,
  "session_id" varchar(16) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "body" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "method" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL,
  "typegw" int8 DEFAULT NULL,
  "parent" int8 DEFAULT NULL
)
;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."vnwalletgw_session_id_seq"
OWNED BY "public"."vnwalletgw_session"."id";
SELECT setval('"public"."vnwalletgw_session_id_seq"', 480, true);
ALTER SEQUENCE "public"."vnwalletgw_transaction_log_id_seq"
OWNED BY "public"."vnwalletgw_transaction_log"."id";
SELECT setval('"public"."vnwalletgw_transaction_log_id_seq"', 2511, true);

-- ----------------------------
-- Primary Key structure for table vnwalletgw_session
-- ----------------------------
ALTER TABLE "public"."vnwalletgw_session" ADD CONSTRAINT "vnwalletgw_session_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table vnwalletgw_transaction_log
-- ----------------------------
ALTER TABLE "public"."vnwalletgw_transaction_log" ADD CONSTRAINT "vnwalletgw_transaction_log_pkey" PRIMARY KEY ("id");